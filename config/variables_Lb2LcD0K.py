
import sys
import math

#import the local libraries
from Constants import *

################

# variables

# i = variable
# [i][0] = name of the variable as encoded in the PhaseSpace and MatrixElement modules:
#          please DO NOT tough them, or most likely you will get a crash as result
# [i][1] = name of the variable as it is in the input sample,
#          it will be used to name the output files and plots
# [i][2], [i][3] = min and max of the x axis
# [i][4] = title of the plot
# [i][5] = units

# main variables, which describe the decay
variables = [
    ["m_d1d2", "MLcD0",
     4.15, 5.13,     # axis range [GeV/c^2]
     #Lc.mass + D0.mass, Lb.mass - K.mass,                # axis range
     "#it{m}(#it{#Lambda}^{#plus}_{c}#it{#bar{D}^{0}})",  # title
     "GeV/#it{c}^{2}",    # units
     "left"],             # position of the legend

    ["cosThetaM", "cosThetaLb",
     -1, +1,                          # axis range
     "cos#it{#theta_{#Lambda_{b}}}",  # title
     "",                  # units
     "right"],            # position of the legend
    
    ["cosThetaA", "cosThetaA",
     -1, +1,                # axis range
     "cos#it{#theta_{P_{c}}}",  # title
     "",                  # units
     "right"],            # position of the legend

    ["phid1", "phiLc",
     -3.14, 3.14,          # axis range
     "#it{#phi_{#Lambda_{c}}}",  # title
     "",                  # units
     "right"],            # position of the legend
]

# additional variables to plot
other_variables = [
    ["m_d2d3", "MD0K",
     2.41, 3.28,   # axis range [GeV/c^2]
     #D0.mass + K.mass + 0.5, Lb.mass - Lc.mass - 0.5,    # axis range
     "#it{m}(#it{#bar{D}^{0}}#it{K}^{#minus})",           # title
     "GeV/#it{c}^{2}",                                   # units
     "right"],            # position of the legend
    
    ["m_d1d3", "MLcK",
     2.83, 3.7,   # axis range [GeV/c^2]
     #Lc.mass + K.mass + 0.5, Lb.mass - D0.mass - 0.5,  # axis range
     "#it{m}(#it{#Lambda}^{+}_{c}#it{K}^{#minus})",     # title
     "GeV/#it{c}^{2}",                                 # units
     "right"],            # position of the legend
    
    ["cosThetaB", "cosThetaB",
     -1, +1,                # axis range
     "cos#it{#theta_{D^{*}_{s}}}",  # title
     "",                   # units
     "right"],            # position of the legend
    
    ["cosThetaC", "cosThetaC",
     -1, +1,                # axis range
     "cos#it{#theta_{#chi_{c}}}",  # title
     "",                   # units
     "left"],            # position of the legend
    
    ["phid1_B", "phiLc_B",
     -3.14, 3.14,              # axis range
     "#it{#phi_{#Lambda_{c},D^{*}_{s}}}",    # title
     "",                            # units
     "right"],            # position of the legend
    
    ["phid2_B", "phiD0_B",
     -3.14, 3.14,              # axis range
     "#it{#phi_{#bar{D}^{0}_{D^{*}_{s}}}}",  # title
     "",                            # units
     "right"],            # position of the legend
    
    ["phid2_C", "phiD0_C",
     -3.14, 3.14,              # axis range
     "#it{#phi_{#bar{D}^{0}_{#chi_{c}}}}",  # title
     "",                            # units
     "right"],            # position of the legend
    
    ["phid1_C", "phiLc_C",
     -3.14, 3.14,              # axis range
     "#it{#phi_{#Lambda_{c},#chi_{c}}}",    # title
     "",                            # units
     "right"],            # position of the legend
    
]

############

# all the kinematic variables of the particles
variables_kin = [
    ["M_PX", "Lb_PX",
     -100000., 100000.,
     "#it{p_{#Lambda_{b}}}",
     "MeV/c"
    ],

    ["M_PY", "Lb_PY",
     -100000., 100000.,
     "#it{p_{#Lambda_{b}}}",
     "MeV/c"
    ],

    ["M_PZ", "Lb_PZ",
     0., 100000.,
     "#it{p_{#Lambda_{b}}}",
     "MeV/c"
    ],

    ["M_E", "Lb_E",
     0., 100000.,
     "#it{E_{#Lambda_{b}}}",
     "MeV"
    ],

    ["d1_PX", "Lc_PX",
     -100000., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["d1_PY", "Lc_PY",
     -100000., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["d1_PZ", "Lc_PZ",
     0., 100000.,
     "#it{p_{#Lambda_{c}}}",
     "MeV/c"
    ],

    ["d1_E", "Lc_E",
     0., 100000.,
     "#it{E_{#Lambda_{c}}}",
     "MeV"
    ],

    ["d2_PX", "D0_PX",
     -100000., 100000.,
     "#it{p_{D^{0}}}",
     "MeV/c"
    ],

    ["d2_PY", "D0_PY",
     -100000., 100000.,
     "#it{p_{D^{0}}}",
     "MeV/c"
    ],

    ["d2_PZ", "D0_PZ",
     0., 100000.,
     "#it{p_{D^{0}}}",
     "MeV/c"
    ],

    ["d2_E", "D0_E",
     0., 100000.,
     "#it{E_{D^{0}}}",
     "MeV"
    ],

    ["d3_PX", "K_PX",
     -100000., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_PY", "K_PY",
     -100000., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_PZ", "K_PZ",
     0., 100000.,
     "#it{p_{K^{-}}}",
     "MeV/c"
    ],

    ["d3_E", "K_E",
     0., 100000.,
     "#it{E_{K^{-}}}",
     "MeV"
    ]
]

