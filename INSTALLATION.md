
# Installation
This is a list of the most important software packages required by the analysis:
- Python 2.7 AND Python 3.*;
- Tensorflow 1.4 or greater, with Python 2.7 support;
- ROOT 6.*, with Roofit and Minuit components;
- PyROOT with Python 2 support;
- (Mini)Conda package manager.

Step-by-step instructions how to install this bunch of packages,
and some other libraries not specified here, follow.
Please be careful handling correctly the Pyton 2 vs 3 installations:
some components need Python 2, others Python 3.

## Install the conda package manager 
Download and install `miniconda` with python2.7 as described [here](http://conda.pydata.org/docs/install/quick.html). For example:

```
 wget https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh
 chmod +x Miniconda-latest-Linux-x86_64.sh
 ./Miniconda-latest-Linux-x86_64.sh
```

Test your installation:
```
conda --version
conda update conda
```

## Make a conda environment with snakemake
We get snakemake from the bioconda stream. It needs python3. We also need the Python libraries beautiful-soup and pyyaml.
Create a conda environment called `snake` to hold the installation. And install several further useful libraries and tools

```
conda create -n snake -c bioconda python=3.4 snakemake beautiful-soup pyyaml
conda install -n snake -c https://conda.anaconda.org/conda-forge gitpython
conda install -n snake -c https://conda.anaconda.org/anaconda pytz
conda install -n snake -c anaconda numpy
```

If you don't have the Boost libraries installed on your system (this is the case of the xi0 cluster in Heidelberg),
install it in the snake environment, and remember to set the env variable to point there:

```
conda install -n snake boost=1.61.0
export BOOST_ROOT=$HOME/miniconda2/envs/snake/lib
```

note that here it's assumed you have Miniconda installed in your `HOME` folder.

Now point python2.7 to its local installation of Miniconda, and proceed installing other packages which are working with Python 2.7 :

```
alias python2.7=$HOME/miniconda2/bin/python2.7

source activate snake
python2.7 -m pip install --ignore-installed --upgrade https://pypi.python.org/packages/be/50/9793ed6c8f59bc19427088482bd69a46e0594d7e10bad841fe4dea3fc4cd/PyPDT-0.7.4.tar.gz
python2.7 -m pip install sympy
```

## Install TensorFlow

Detailed instructions are provided in this repo:

https://gitlab.cern.ch/apiucci/TensorFlowAnalysis

But in the simplest case of running over CPU only, this is enough
(remember to point to your local Python 2.7 installation of Miniconda, like in the instructions above):

```
source activate snake
python2.7 -m pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.6.0-cp27-none-linux_x86_64.whl
```

and it may work. Please notice that that installation rely on Python 2.7 libraries.
