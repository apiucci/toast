#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
# ==============================================================================

import tensorflow as tf
import numpy as np
import ROOT
import sys
import math

from ROOT import TGraph, TCanvas

#import the local libraries
import Interface
from Interface import Const, Complex, CastComplex, Sqrt, Abs, Zeros

import Utils
from Constants import *

# import the Python settings
from python_settings import *

sess = None

# Blatt-Weisskopf factors, in this formalism
# the index is the orbital momentum L of the decay
def get_BlattWeisskopf(q_a_, q_0, L) :

    # just for testing, forget the Blatt-Weisskopf factors
    return Const(1.)
    
    if L == 0 :
        return Const(1.)
    elif L == 1:
        return Const(Sqrt(2 / (q_a + q_0)))
    elif L == 2:
        return Const(Sqrt(13 / ((q_a - 3*q_0)**2 + 9*q_a*q_0)))
    else :
        logging.error("KMatrix::get_BlattWeisskopf: Error: L value not implemented. L = {}".format(L))
        exit(1)
        
# breakup momentum
def q(s, m1, m2) :
    lower = m1 - m2
    upper = m1 + m2

    return Sqrt((s - lower**2) * (s - upper**2)/(4*s))
    
# two-body phase space factor
def rho(s, m1, m2) :
    
    return (1. / (8*math.pi)) * q(s, m1, m2) / Sqrt(s)

# angular momentum barrier
# M = pole mass, m1 and m2 = masses of the decay products of M,
# L = the orbital angular momentum of the decay
def gamma(s, M, m1, m2, L) :
    q_a = q(s, m1, m2)
    q_0 = q(M**2, m1, m2)

    # remember that angular momenta are doubled!
    return q_a**(L/2) #* get_BlattWeisskopf(q_a, q_0, L/2)

#####################

class KMatrix:
    def __init__(self, s = [],
                 num_channels = 0,
                 channel = 0,
                 resonance_masses = [],
                 num_res = 0,
                 alpha_couplings = [],
                 g_couplings = [],
                 width_factors = [],
                 daughters_masses = [],
                 L_channels = [],
                 background_terms = []) : 

        # number of channels
        self.num_channels = num_channels

        # channel of which you are interested to get the amplitude
        self.channel = channel
        
        # list of resonance masses
        # size = number of resonances (== num_res)
        self.resonance_masses = resonance_masses
        self.num_res = num_res
        
        # list of mass pairs (we only consider two-body decays...) of the channel daughters:
        # a pair of masses for each channel
        # size = num_channels * 2
        self.daughters_masses = daughters_masses

        # list of orbital angular momenta of the decays of the channels
        # size = num_channels
        self.L_channels = L_channels

        # matrix of couplings of the resonances to the channels
        # size = num_res * num_channels
        self.g_couplings = g_couplings

        # matrix of couplings of the mother decay to the resonances
        # size = num_res * 1
        self.alpha_couplings = alpha_couplings 

        # list of background terms, used in the production vector P
        # size = num_channels
        self.background_terms = background_terms

        # widths factors of the sigma vector
        self.width_factors = width_factors
        
        # cast s to be complex, just to be sure
        s = CastComplex(s)
        
        # set the matrices and vectors
        self.set_V_matrix(s)
        self.set_sigma(s)
        self.set_P(s)
        
    # set the V matrix (which is the actual K matrix...)
    def set_V_matrix(self, s) :

        # initialise the V matrix to 0
        # size = num_channels * num_channels
        self.V_matrix = np.zeros(shape = (self.num_channels, self.num_channels), dtype = object)
        
        # fill the V matrix
        for i in range(self.num_channels) :
            for j in range(self.num_channels) :
                
                temp_sum = CastComplex(0.)
                
                # sum over the resonances
                for k in range(self.num_res) :            
                    temp_sum -= (self.g_couplings[k][i] * self.g_couplings[k][j]) / (s - self.resonance_masses[k]**2)
                    
                self.V_matrix[i][j] = temp_sum
        
        return

    # set the sigma row vector
    def set_sigma(self, s) :
        
        # initialise the Sigma row vector
        # size = 1 * num_channels
        self.sigma = np.zeros(shape = (1, self.num_channels), dtype = object)
        
        # loop over the channels
        for i in range(self.num_channels) :

            # masses of the channel daughters
            m_1 = self.daughters_masses[i][0]
            m_2 = self.daughters_masses[i][1]
            L = self.L_channels[i]

            # this term is only used to get the Blatt-Weiskoppf term in the gamma vector
            # so far these terms are not used here:
            # let's put a reference value
            M = CastComplex(-999.)

            # sigma = i * (rho * gamma + width_factor)
            self.sigma[0][i] = Complex(np.float64(0.), np.float64(1.)) * (rho(s, m_1, m_2) * gamma(s, M, m_1, m_2, L)**2 + self.width_factors[i])
                     
        return 

    # set the production vector P
    # size = num_channels * 1
    def set_P(self, s) :

        # initialise the production vector
        self.P = np.zeros(shape = (self.num_channels, 1), dtype = object)
        
        # loop over the channels
        for i in range(self.num_channels) :
            
            # put the background term
            temp_sum = CastComplex(self.background_terms[i])

            # loop over the resonances
            for j in range(self.num_res) :
                temp_sum -= CastComplex(self.g_couplings[j][i]) * self.alpha_couplings[j][0] / (CastComplex(s) - CastComplex(self.resonance_masses[j])**2)

            self.P[i][0] = temp_sum
        
        return

    # get the V matrix
    def get_V_matrix(self) :
        return self.V_matrix

    # get gamma
    def get_sigma(self) :
        return self.sigma

    # get the production vector
    def get_P(self) :
        return self.P
    
    # get the D matrix = [1 - sigma*V]^-1
    def get_D_matrix(self) :

        # get the V matrix
        V_matrix = self.get_V_matrix()

        # get the sigma row vector
        sigma = self.get_sigma()

        ###########
        
        # compute the V matrix multiplied by sigma
        self.VdotSigma = np.zeros(shape = (self.num_channels, self.num_channels), dtype = object)
        
        temp_matrix = []

        # loop over the elements of the V matrix
        for i in range(self.num_channels) :
            for j in range(self.num_channels) :
                self.VdotSigma[i][j] = V_matrix[i][j] * sigma[0][j]
        
        ###########
                
        # D matrix = identity - V*Sigma
        D_matrix = np.zeros(shape = (self.num_channels, self.num_channels), dtype = object)
        
        # substract VdotSigma to the identity matrix
        for i in range(self.num_channels) :
            for j in range(self.num_channels) :

                if i == j :
                    D_matrix[i][j] = 1 - self.VdotSigma[i][j]
                else :
                    D_matrix[i][j] = -self.VdotSigma[i][j]
        
        # compute the inverse matrix
        inv_Dmatrix = Utils.getMatrixInverse(D_matrix)
        
        return inv_Dmatrix

    # get out this damned amplitude!
    def get_amplitude(self, s, channel) :

        # let be sure about this, once again...
        s = CastComplex(s)
        
        # get the D matrix and the production vertex
        D_prod = self.get_D_matrix()
        P_prod = self.get_P()
        
        # multiply from the right, to the left
        prod_1 = np.zeros(shape = (self.num_channels, 1), dtype = object)
        
        for i in range(self.num_channels) :
            for j in range(self.num_channels) :
                prod_1[i] += D_prod[i][j] * P_prod[j]

        # amplitude
        A = np.zeros(shape = (self.num_channels, 1), dtype = object)
        
        # loop over the channels
        for i in range(self.num_channels) :

            # masses of the channel daughters
            m_1 = self.daughters_masses[i][0]
            m_2 = self.daughters_masses[i][1]
            L = self.L_channels[i]

            # this term is only used to get the Blatt-Weiskoppf term in the gamma vector
            # so far these terms are not used here:
            # let's put a reference value
            M = CastComplex(-999.)
            
            A[i][0] = prod_1[i][0] * gamma(s, M, m_1, m_2, L)
                
        # get the amplitude for the desired channel
        return Abs(A[channel][0])
    
######################

# test the K-matrix machinery, for the Ds(2700) and Ds(2860) states
def test_K_matrix() :

    global sess
    
    # initialise the TensorFlow session                                                                                                                                               
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init)

    # boundaries of the invariant mass distribution to consider
    m_0 = Const(2.4)
    m_1 = Const(3.2)

    # number of points, and mass step
    num_points = 300
    dm = (m_1 - m_0) / float(num_points)

    # resonance masses and number of states
    resonance_masses = np.array([np.complex(2.7, 0.), np.complex(2.860)], dtype = np.complex128)
    num_states = len(resonance_masses)
    
    # alpha couplings
    alpha_couplings = np.array([[np.complex(1., 0.)],
                                [np.complex(0.01, -0.001)]], dtype = np.complex128)

    # g couplings
    # [0][0]: Ds(2700) to D0 K
    # [0][1]: Ds(2700) to D1 K0
    # [1][0]: Ds(2700) to D0 K
    # [1][1]: Ds(2700) to D1 K0
    g_couplings = np.array([[np.complex(1.9, 0.), np.complex(1.5, 0.)],
                            [np.complex(1., 0.), np.complex(0.2, 0.)]],
                           dtype = np.complex128)

    # widths factors used in the sigma vector
    width_factors = np.array([np.complex(0.1, 0.), np.complex(0.1, 0.)],
                             dtype = np.complex128)
        
    # daughters masses for the two channels    
    daughters_masses = np.array([[np.complex(1.86483, 0.), np.complex(0.493677, 0.)],
                                 [np.complex(2.4208, 0.), np.complex(0.493677, 0.)]],
                                dtype = np.complex128)

    # orbital angular momenta for the decays of the two channels
    # remember that they are doubled!
    L_channels = [2, 0]
    
    # background terms
    background_terms = np.array([1., 0.1], dtype = np.float64)
    
    # data: values of the squared mass
    s = np.zeros(num_points, dtype = np.complex128)
    
    # loop over the mass steps
    for i in range(num_points) :
        
        # compute the squared mass for the current bin, and store it
        m = m_0 + i * dm

        s[i] = sess.run(m**2)

    logging.debug("s = {}".format(s))

    s = tf.convert_to_tensor(s)

    # build the K-matrix
    kmatrix = KMatrix(s = s,
                      num_channels = 2,
                      resonance_masses = resonance_masses,
                      num_res = 2,
                      alpha_couplings = alpha_couplings,
                      g_couplings = g_couplings,
                      width_factors = width_factors,
                      daughters_masses = daughters_masses,
                      L_channels = L_channels,
                      background_terms = background_terms)

    # amplitudes of the K matrix
    amplitude = kmatrix.get_amplitude(s = s,
                                      channel = 0)

    # final graph
    graph = TGraph(num_points)

    # loop over the data points
    for i, data in enumerate(sess.run(amplitude)) :

        m = sess.run(m_0 + i * dm)

        # squared amplitude!
        graph.SetPoint(i, m, data**2)

    # draw the graph in a canvas
    canv = TCanvas("", "canv", 800, 600)
    
    canv.cd()
    graph.Draw("APL")

    # save the canvas
    canv.SaveAs("test.pdf")
    
    return

# well, test it!
#test_K_matrix()
