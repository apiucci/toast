#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
#         (based on previous software by Adam Morris)
# ==============================================================================

import math
import sys
import argparse
import numpy as np
import importlib
import itertools
import time
import re

import tensorflow as tf
from tensorflow.python import debug as tf_debug

from ROOT import TFile, TH1F, TH2F, TCanvas

# import the TensorflowAnalysis libraries
import Interface
from Kinematics import *
import Optimisation
import Likelihood
import PDFs

# import the local libraries
import MatrixElement
import PhaseSpace
import Fractions_Interference
import Utils

# import the Python settings
from python_settings import *

# import some other stuff
from variables_Lb2LcD0K import variables

def reweight(x, phase_space, variables, weight):
	data = []

        for i in range(len(variables)):
		data += [tf.transpose(x)[i]]

        data += [weight]
        
	return tf.transpose(data)

if __name__ == "__main__" :

        logging.info("")
        logging.info("-------------------------------")
        logging.info("------------- Fit -------------")
        logging.info("-------------------------------")
        
        ###########
        ## parser
        ###########

        parser = argparse.ArgumentParser(description="Parser of the Fit script")

        # get the input file and tree names of the data sample
        parser.add_argument('--inFile_data_name', dest = 'inFile_data_name',
                            default = 'toy.root', help = "name of the input data sample")
        parser.add_argument('--inTree_data_name', dest = 'inTree_data_name',
                            default = 'toy', help = "tree name of the input data sample")
        
        # get the input file and tree names of the normalisation sample
        parser.add_argument('--inFile_norm_name', dest = 'inFile_norm_name',
                            default = 'toy.root', help = "name of the input normalisation sample")
        parser.add_argument('--inTree_norm_name', dest = 'inTree_norm_name',
                            default = 'toy_normalisation', help = "tree name of the input normalisation sample")
        
        # get the input file where the initial values of the parameters are specified
        parser.add_argument('--in_params', dest='in_params_file_name',
                            default = "", help = "name of the input file containing the initial parameter values")

        # get the input file where the efficiency maps are placed, and the histo name
        parser.add_argument('--inFileEff_name', dest='inFileEff_name',
                            default = "", help = "name of the input file containing the efficiency map")
        parser.add_argument('--inHistoEff_name', dest='inHistoEff_name',
                            default = "", help = "name of the efficiency histogram")

        parser.add_argument('--inFileEff_2_name', dest='inFileEff_2_name',
                            default = "", help = "name of the input file containing the (second) efficiency map")
        parser.add_argument('--inHistoEff_2_name', dest='inHistoEff_2_name',
                            default = "", help = "name of the (second) efficiency histogram")

        parser.add_argument('--eff_var_1', dest='eff_var_1',
                            default = "", help = "name of the (first) variable in which the efficiency histogram is binned")

        parser.add_argument('--eff_var_2', dest='eff_var_2',
                            default = "", help = "name of the (second) variable in which the efficiency histogram is binned")
                
        parser.add_argument('--smooth_histoEff', dest='smooth_histoEff',
                            default = False, help = "do I have to smooth the efficiency histos?")
                
	# get the config file name where the resonances to be fitted are described
	parser.add_argument('--config_name', dest = 'config_name',
			    default = 'toy', help = "name of config file where the resonances are described")

        # get the names of the weights associated to the signal and normalisation events
        parser.add_argument('--sig_weight_name', dest='sig_weight_name',
                            default = "", help = "name of the weight assigned to the signal events. Keep it empty if they are not weighted")
        parser.add_argument('--norm_weight_name', dest='norm_weight_name',
                            default = "", help = "name of the weight assigned to the normalisation events. Keep it empty if they are not weighted")
	
        # set the output file name
        parser.add_argument('--outFile_name', dest = 'outFile_name',
                            default = './', help = "output file name")

        # 'randomly' add a latency time, if "seed" is present in the inFile name:
        # tuned for running over many toy runs with snakemake --cores 4 on a 8-threads system
        parser.add_argument('--random_latency', dest = 'random_latency',
                            default = False, help = '"randomly" add a latency time, if "seed" is present in the inFile name')
                
        # now get the parsed options
        args = parser.parse_args()

        inFile_data_name = args.inFile_data_name
        inTree_data_name = args.inTree_data_name
        inFile_norm_name = args.inFile_norm_name
        inTree_norm_name = args.inTree_norm_name
        in_params_file_name = args.in_params_file_name
        inFileEff_name = args.inFileEff_name
        inHistoEff_name = args.inHistoEff_name
        inFileEff_2_name = args.inFileEff_2_name
        inHistoEff_2_name = args.inHistoEff_2_name
        eff_var_1 = args.eff_var_1
        eff_var_2 = args.eff_var_2
        smooth_histoEff = args.smooth_histoEff
	config_name = args.config_name
        outFile_name = args.outFile_name
        sig_weight_name = args.sig_weight_name
        norm_weight_name = args.norm_weight_name
        random_latency = args.random_latency
        
        # print the parsed options
        logging.info("\n")
        logging.info("inFile_data_name = {}".format(inFile_data_name))
        logging.info("inTree_data_name = {}".format(inTree_data_name))
        logging.info("inFile_norm_name = {}".format(inFile_norm_name))
        logging.info("inTree_norm_name = {}".format(inTree_norm_name))
        logging.info("in_params_file_name = {}".format(in_params_file_name))
        logging.info("inFileEff_name = {}".format(inFileEff_name))
        logging.info("inHistoEff_name = {}".format(inHistoEff_name))
        logging.info("inFileEff_2_name = {}".format(inFileEff_2_name))
        logging.info("inHistoEff_2_name = {}".format(inHistoEff_2_name))
        logging.info("eff_var_1 = {}".format(eff_var_1))
        logging.info("eff_var_2 = {}".format(eff_var_2))
        logging.info("smooth_histoEff = {}".format(smooth_histoEff))
	logging.info("config_name = {}".format(config_name))
	logging.info("outFile_name = {}".format(outFile_name))
        logging.info("sig_weight_name = {}".format(sig_weight_name))
        logging.info("norm_weight_name = {}".format(norm_weight_name))
        logging.info("random_latency = {}".format(random_latency))

        if random_latency :
                # for toys (having the "seed<number>_" in their input file name),
                # 'randomly' add a 3-minutes delay
                # this is to optimise the CPU usage on 8-threads systems when running snakemake --cores 4
                seed_number = re.search('seed(.+?).', inFile_data_name)
                
                if seed_number :
                        if int(seed_number.group(1)) % 2 :
                                time.sleep(180)
                        
        # initialise the TensorfFlow session
	config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        
        sess = tf.Session(config=config)
        
        # initialise the variables
	init = tf.global_variables_initializer()
        sess.run(init)
                
        ##########
        ## get the efficiencies, and smooth them if required
        ##########
        
        eff_shape = None
        
        # check if I have to take into account the efficiency
        if inFileEff_name != "" :

                # open the input file
                inFileEff = TFile.Open(inFileEff_name)

                # check that the file exists
                if not inFileEff :
                        logging.error("Error: the efficiency file {} does not exist.\n".format(inFileEff_name))
                        sys.exit(1)

                # get the eff histo
                effHisto = inFileEff.Get(inHistoEff_name)

                # check that the histo exists
                if not effHisto :
                        logging.error("Error: the efficiency histo {} does not exist.\n".format(inHistoEff_name))
                        sys.exit(1)

                # close the input file
                effHisto.SetDirectory(0)
                inFileEff.Close()

                #smooth the efficiency histo
                if smooth_histoEff :
                        effHisto.Smooth()
                        logging.info("Efficiency histo {} succesfully smoothed.".format(inHistoEff_name))
                        
                # shape the efficiency histo
                eff_shape = PDFs.RootHistShape(effHisto)

                logging.info("Efficiency histo {} succesfully shaped.\n".format(inHistoEff_name))

        # the same for the second efficiency map
        eff_shape_2 = None
        
        if inFileEff_2_name != "" :

                # open the input file
                inFileEff_2 = TFile.Open(inFileEff_2_name)

                # check that the file exists
                if not inFileEff_2 :
                        logging.error("Error: the efficiency file {} does not exist.\n".format(inFileEff_2_name))
                        sys.exit(1)

                # get the eff histo
                effHisto_2 = inFileEff_2.Get(inHistoEff_2_name)

                # check that the histo exists
                if not effHisto_2 :
                        logging.error("Error: the efficiency histo {} does not exist.\n".format(inHistoEff_2_name))
                        sys.exit(1)
                        
                # close the input file
                effHisto_2.SetDirectory(0)
                inFileEff_2.Close()
                
                #smooth the efficiency histo
		if smooth_histoEff :
                        effHisto_2.Smooth()
			logging.info("Efficiency histo {} succesfully smoothed.".format(inHistoEff_2_name))

                # shape the efficiency histo
                eff_shape_2 = PDFs.RootHistShape(effHisto_2)
                
                logging.info("Efficiency histo {} succesfully shaped.\n".format(inHistoEff_2_name))
                
        ############

        # set the a few sessions
        MatrixElement.sess = sess
        PhaseSpace.sess = sess
	Utils.sess = sess

        phase_space = PhaseSpace.ThreeBodyPhaseSpace()
        
        MatrixElement.parsed_parameters = {}
        
        nprojects = 100000
        
        # import the resonances from the config file
        sys.path.append(os.path.dirname(config_name))
        resonance_module = importlib.import_module(os.path.basename(config_name)[:-3])  #-3 removes the '.py' at the end of the string

        non_res = resonance_module.non_res
        A = resonance_module.A
        B = resonance_module.B
        C = resonance_module.C
        M_d1_hel = resonance_module.M_d1_hel
        
        resonances = A + B + C

        start = time.time()
        
        # let's reinitialise the variables
        init = tf.global_variables_initializer()
        sess.run(init)
        
        logging.info("")
        logging.info("Imported resonances:")
        logging.info("A = {}".format([A_res.name for A_res in A]))
        logging.info("B = {}".format([B_res.name for B_res in B]))
        logging.info("C = {}\n".format([C_res.name for C_res in C]))
        
        # list of switches (flags that control the components of the PDF)
        # +1 stands for the non-resonant contribution
        switches = Optimisation.Switches(len(resonances) + 1)
        
        def event_weight(datapoint, normalisation = 1.):
		return tf.transpose(datapoint)[-1] * normalisation  # dangerous: assume the weight
                                                                    # is the last element of the list
        
        def model(datapoint, component_index = -1,
                  eff_shape = None, eff_shape_2 = None,
                  eff_var_1 = None, eff_var_2 = None):
		return MatrixElement.MatrixElement(phase_space, datapoint, switches,
                                                   component_index,
                                                   resonances = resonances,
                                                   non_res = non_res,
                                                   M_d1_hel_ = M_d1_hel,
                                                   eff_shape = eff_shape,
                                                   eff_shape_2 = eff_shape_2,
                                                   eff_var_1 = eff_var_1,
                                                   eff_var_2 = eff_var_2)
        
        # build the PDFs
        data_placeholder = phase_space.data_placeholder
	norm_placeholder = phase_space.norm_placeholder

        # no efficiency correction to the data pdf
	data_pdf = model(data_placeholder, component_index = -1,
                         eff_shape = None, eff_shape_2 = None)

        # efficiency correction to the for the normalisation integral
	norm_pdf = model(norm_placeholder,
                         eff_shape = eff_shape, eff_shape_2 = eff_shape_2,
                         eff_var_1 = eff_var_1,
                         eff_var_2 = eff_var_2)
        
        # set the initial parameter values from an external file
        if in_params_file_name != "" :
                new_values = Optimisation.ReadParameters(in_params_file_name)
                Optimisation.SetParameters(new_values)
                
        # print the parameter values
        Optimisation.PrintParameters()
        
        logging.info("Loading the data from: {}".format(inFile_data_name))

        input_weight_name = []
        
        if sig_weight_name != "":
                input_weight_name = [sig_weight_name]
                
        # get the data, and the sweights
        unfiltered_data_sample = Utils.GetData([var[1] for var in variables] + input_weight_name,
                                               inFile_data_name, inTree_data_name)
        
        # filter the data, checking for the allowed phase space
        data_sample = sess.run(phase_space.Filter(unfiltered_data_sample))

        logging.info("Loading the normalisation sample from: {}".format(inFile_norm_name))

        input_weight_name = []

        if norm_weight_name != "":
		input_weight_name = [norm_weight_name]
                
        # get the normalisation sample, and the weights
        unfiltered_norm_sample = Utils.GetData([var[1] for var in variables] + input_weight_name,
                                               inFile_norm_name, inTree_norm_name)
        
        # filter the normalisation sample, checking for the allowed phase space
        norm_sample = sess.run(phase_space.Filter(unfiltered_norm_sample))
        
        # reweight the normalisation sample, if needed
        if norm_weight_name != "":
                logging.info("Fit:: ALERT: Reweighting simulation: this has never been tested before! Do it at your own risk")
                norm_sample = sess.run(reweight(norm_placeholder, phase_space, variables, event_weight(norm_placeholder)),
                                       {norm_placeholder: norm_sample})

        ############
        
        # save out the shaped efficiency maps

        if eff_shape != None :

                h_shaped = TH2F("h_shaped", "", effHisto.GetNbinsX(), effHisto.GetXaxis().GetXmin(), effHisto.GetXaxis().GetXmax(),
                                effHisto.GetNbinsY(), effHisto.GetYaxis().GetXmin(), effHisto.GetYaxis().GetXmax())
		
                # hopefully "eval" works correctly...
                shape = sess.run(eff_shape.shape(tf.stack([eval("phase_space." + eff_var_1 + "(data_sample)"),
                                                           eval("phase_space." + eff_var_2 + "(data_sample)")],
                                                                axis = 1)))
                
		for i in range(effHisto.GetNbinsX()) :
                        for j in range(effHisto.GetNbinsY()) :
                                h_shaped.SetBinContent(i+1, j+1, shape[effHisto.GetNbinsX()*j + i])

                #canv = TCanvas("canv", "", 800, 600)
		#canv.cd()
                #h_shaped.Draw("zcol")
                #canv.SaveAs(outFile_name + "_effshape_1.pdf")
                
        if eff_shape_2 != None :
                
                h_shaped = TH2F("h_shaped", "", effHisto_2.GetNbinsX(), effHisto_2.GetXaxis().GetXmin(), effHisto_2.GetXaxis().GetXmax(),
                                effHisto_2.GetNbinsY(), effHisto_2.GetYaxis().GetXmin(), effHisto_2.GetYaxis().GetXmax())
                
                # hopefully "eval" works correctly...
                shape = sess.run(eff_shape_2.shape(tf.stack([eval("phase_space." + eff_var_1 + "(data_sample)"),
                                                             eval("phase_space." + eff_var_2 + "(data_sample)")],
                                                            axis = 1)))
                
	        for i in range(effHisto_2.GetNbinsX()) :
                        for j in range(effHisto_2.GetNbinsY()) :
                                h_shaped.SetBinContent(i+1, j+1, shape[effHisto_2.GetNbinsX()*j + i])

                #canv = TCanvas("canv", "", 800, 600)
		#canv.cd()
	        #h_shaped.Draw("zcol")
                #canv.SaveAs(outFile_name + "_effshape_2.pdf")
                
        ##########
        ## perform the fit to the data
        ##########

        integral = None
        
        if norm_weight_name == "" :                           
                integral = Likelihood.Integral(norm_pdf)
        else :
                # normalisation of the weights used for reweighting
                reweighting_normalisation = sum([dp[-1] for dp in norm_sample])/sum([dp[-1]**2 for dp in norm_sample])

                # normalisation integral
                integral = Likelihood.WeightedIntegral(norm_pdf, event_weight(norm_placeholder))

                # normalise the integral
                integral /= reweighting_normalisation
                
        nll = None
        
        if sig_weight_name == "" :
                # the UnbinnedExtended likelihood (without weighting) seems to have some problem
                #nll = Likelihood.UnbinnedExtendedNLL(data_pdf, integral)

                nll = Likelihood.UnbinnedNLL(data_pdf, integral)
        else :
                # normalisation factor to propagate properly the errors
	        # the s-weight is the last element of the data points
                # sweights_normalisation = sum_i sweight_i / sum_i (sweight_i)^2
                sweights_normalisation = sum([dp[-1] for dp in data_sample])/sum([dp[-1]**2 for dp in data_sample])
                
                # get the negative log of the unbinned weighted --> extendend <-- likelihood
                nll = Likelihood.UnbinnedWeightedExtendedNLL(pdf = data_pdf, integral = integral,
                                                             weight = event_weight(data_placeholder),   # get the s-weight of the event,
                                                             weight_norm = sweights_normalisation)      # multiplied by a normalisation factor
                                                                                                        # to propagate properly the errors
                                                     
        # perform the fit: cross the fingers
        result, correlation_dict, covariance_dict = Optimisation.RunMinuit(sess, nll, {data_placeholder: data_sample, norm_placeholder: norm_sample},
                                                                           tmpFile = outFile_name + "_tmp_results.txt",
                                                                           call_limit = Minuit_call_limit,
                                                                           useGradient = True)
        
        # write out the fit results
        Optimisation.WriteFitResults(result, outFile_name + "_results.info")
        
        # write out the correlations and covariances
        Optimisation.WriteFitCorrelations(correlation_dict, outFile_name + "_correlations.info")
        Optimisation.WriteFitCovariances(covariance_dict, outFile_name + "_covariances.info")

        # write out the correlations as latex table
        Fractions_Interference.WriteMatrixResultsLatex(correlation_dict, outFile_name + "_correlations.tex")
        
        ##########
        ## compute and write out the fit fractions and the interference terms
        ##########

        # update the Fractions_Interference module with the measured Lb and Lc helicity factors
        # I know, that's an horrible hack: but it's hopefully only temp
        Fractions_Interference.M_d1_hel = M_d1_hel
        
        # total PDF integral over the normalisation sample
        #pdf_norm = sess.run(norm_pdf, feed_dict = {norm_placeholder : norm_sample} )
        #total_int = np.sum(pdf_norm)
        
        fit_fractions, total_int = Fractions_Interference.WriteFitFractionsWithErrors(sess, phase_space, data_pdf, data_placeholder,
                                                                                      switches, norm_sample,
                                                                                      resonances, outFile_name,
                                                                                      errors = True, results = result)

        interference_dict = None
        
        # compute the interference terms only if you have more then one resonance
        if len(resonances) > 1 :
                interference_dict = Fractions_Interference.WriteInterferences(sess, phase_space, total_int, norm_sample,
                                                                              resonances, outFile_name)

                # write out the fit results as latex table
                resonance_names = [res.tex_name for res in resonances]
                resonance_names += ["non-resonant"]
                
                Fractions_Interference.WriteFitFractionsLatex(resonance_names, fit_fractions,
                                                              outFile_name + "_fractions.tex",
                                                              print_errors = False)
                
                # write out the interference terms as latex table
                Fractions_Interference.WriteMatrixResultsLatex(interference_dict, outFile_name + "_interference.tex")
        
        #############
        ## Save out the B_LS couplings
        #############
        
	# print the B_LS couplings of the resonances, for debug
        for res in resonances:
	        Utils.print_BLS(res)

	# open the output files with the parameters
        outFile_params_cartesian = open(outFile_name + "_params_cartesian.info", "w")
        outFile_params_polar = open(outFile_name + "_params_polar.info", "w")

        # write out the values of the B_LS couplings
	# and the size of the non-resonant contribution
        # in both cartesian and polar coordinates
        # ...and the M-d1 helicity factors as well, why not
        [Utils.SaveBLS_cartesian(res, outFile_params_cartesian) for res in resonances]
	non_res.SaveNonResSize(outFile_params_cartesian)
        Utils.Save_M_d1_helicities(outFile_params_cartesian, MatrixElement.M_d1_hel)
        [Utils.Save_helcouplings(res, outFile_params_cartesian) for res in resonances]
        [Utils.Save_MassAndWidths(res, outFile_params_cartesian) for res in resonances]
        
        [Utils.SaveBLS_cartesian(res, outFile_params_polar) for res in resonances]
        non_res.SaveNonResSize(outFile_params_polar)
        Utils.Save_M_d1_helicities(outFile_params_polar, MatrixElement.M_d1_hel)
        [Utils.Save_helcouplings(res, outFile_params_polar) for res in resonances]
        [Utils.Save_MassAndWidths(res, outFile_params_polar) for res in resonances]
        
        ###########
        ## Save out all the parameters of the amplitude model
        ###########

        Utils.SaveAllParams(outFile_name + "_allmodelparams.info", resonances = resonances, nonres = non_res)
        
        # dump the amplitude model to a config file
        Utils.DumpModel(A_res = A, B_res = B, C_res = C, non_res = non_res,
                        M_d1_hel = MatrixElement.M_d1_hel,
                        parsed_parameters = MatrixElement.parsed_parameters,
	                outFile_name = outFile_name + "_model_config.py",
                        for_replacing = False)

        # same thing, but with the fit values replaced by "<parameter_name>_VALUE",
        # in order to be easily replaced by sed magic
        Utils.DumpModel(A_res = A, B_res = B, C_res = C, non_res = non_res,
                        M_d1_hel = MatrixElement.M_d1_hel,
                        parsed_parameters = MatrixElement.parsed_parameters,
                        outFile_name = outFile_name + "_model_config_for_replacing.py",
                        for_replacing = True)

        # dump the parsed parameters in a dedicated file
        Utils.DumpParsedParameters(MatrixElement.parsed_parameters, outFile_name + "_parsed_parameters.info")

        # dump the helicity amplitudes
        Utils.DumpHelicityAmplitude(A + B + C, outFile_name + "_helicity_amplitudes.info")
        
        # print out the total execution time
        end = time.time()
        logging.info("Fit:: Execution time = {} min".format(str((end - start)/60.)))

        # close the session
        sess.close()

        
