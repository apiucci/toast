#
# Copyright 2019 CERN for the benefit of the LHCb collaboration
#
# Licensed under the LCPGL License, Version 3 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License here:
#
#     https://opensource.org/licenses/LGPL-3.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IT IS" basis,
# without warranties or conditions of any kind, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Author: Alessio Piucci, alessio.piucci@cern.ch
#         (based on previous software by Adam Morris)
# ==============================================================================

import math
import sys
import importlib
import numpy as np
import time
import re

import tensorflow as tf

from tensorflow.python import debug as tf_debug

from ROOT import TFile, TH1F, TCanvas

# import the local libraries
import MatrixElement
import PhaseSpace
import Fractions_Interference

import Utils

# Tensorflow stuff
import Optimisation
import Interface
import ToyMC

# import the Python settings
from python_settings import *

from Constants import *
from variables_Lb2LcD0K import *

import numpy
#numpy.set_printoptions(threshold=numpy.nan)

import argparse

# import the Python settings
from python_settings import *

#configuration to run over multiple threads in parallel... in principle, at least
session_conf = tf.ConfigProto(
        intra_op_parallelism_threads=4,
        inter_op_parallelism_threads=4
)


if __name__ == "__main__" :
        
        logging.info("")
        logging.info("--------------------------------")
        logging.info("--------- GenerateToys ---------")
        logging.info("--------------------------------")
        
        ###########
        ## parser
        ###########
        
        parser = argparse.ArgumentParser(description="Parser of the GenerateToys script")
        
        # get the config file name where the resonances to be fitted are described
        parser.add_argument('--configFile_name', dest='configFile_name',
                            default = '../config/amplitude_fits/resonances_gentoyMC.py', help="name of the config file with the resonances to simulate")

        # get the output file names
        parser.add_argument('--outFile_name', dest='outFile_name',
                            default = 'toy.root', help="name of the output toy MC file")
        parser.add_argument('--outTree_name', dest='outTree_name',
                            default = 'toy', help="name of the output toy MC tree")
        parser.add_argument('--outFile_params_name', dest='outFile_params_name',
                            default = 'out_params.info', help="proto name of the output logs containing the generated parameters")

        # do you want to use a random generation seed, or an external one?
        parser.add_argument('--random_seed', dest='random_seed',
                            default = False, help="use random seed for the toy generation?")
        parser.add_argument('--seed', dest='seed',
                            default = 1, help="set the seed used by the toy generation")

        # number of signal and normalisation events to simulate
        parser.add_argument('--n_events', dest='n_events',
                            default = 50000, help = "number of signal events to generate")
        parser.add_argument('--n_events_norm', dest='n_events_norm',
                            default = 100000, help = "number of events of the normalisation sample to generate")

        # get the names of the weights to associate to the signal and normalisation events
        parser.add_argument('--sig_weight_name', dest='sig_weight_name',
                            default = "", help = "name of the weight to assign to the signal events. Keep it empty if you don't want to weight them")
        parser.add_argument('--norm_weight_name', dest='norm_weight_name',
                            default = "", help = "name of the weight to assign to the normalisation events. Keep it empty if you don't want to weight them")

        # in case you want to apply to the simulate sample an external efficiency factor,
        # set the input file and histo names of the additional efficiency
        parser.add_argument('--inFileAdditionalEff_name', dest = 'inFileAdditionalEff_name',
                            default = "", help = "input file name of the additional efficiency factor")
        parser.add_argument('--inHistoAdditionalEff_suffix', dest = 'inHistoAdditionalEff_suffix',
                            default = "", help = "suffix of the histo name of the additional efficiency factor")


        # 'randomly' add a latency time, if "seed" is present in the outFile name:
        # tuned for running over many toy runs with snakemake --cores 4 on a 8-threads system
        parser.add_argument('--random_latency', dest = 'random_latency',
                            default = False, help = '"randomly" add a latency time, if "seed" is present in the outFile name')
        
        # now get the parsed options
        args = parser.parse_args()

        configFile_name = args.configFile_name
        outFile_name = args.outFile_name
        outTree_name = args.outTree_name
        outFile_params_name = args.outFile_params_name
        random_seed = args.random_seed
        seed = int(args.seed)
        n_events = int(args.n_events)
        n_events_norm = int(args.n_events_norm)
        sig_weight_name = args.sig_weight_name
        norm_weight_name = args.norm_weight_name
        inFileAdditionalEff_name = args.inFileAdditionalEff_name
        inHistoAdditionalEff_suffix = args.inHistoAdditionalEff_suffix
        random_latency = args.random_latency
        
        # print the parsed options
        logging.info("\n")
        logging.info("configFile_name = {}".format(configFile_name))
        logging.info("outFile_name = {}".format(outFile_name))
        logging.info("outTree_name = {}".format(outTree_name))
        logging.info("outFile_params_name = {}".format(outFile_params_name))
        logging.info("random_seed = {}".format(random_seed))
        logging.info("seed = {}".format(seed))
        logging.info("n_events = {}".format(n_events))
        logging.info("n_events_norm = {}".format(n_events_norm))
        logging.info("sig_weight_name = {}".format(sig_weight_name))
        logging.info("norm_weight_name = {}".format(norm_weight_name))
        logging.info("inFileAdditionalEff_name = {}".format(inFileAdditionalEff_name))
	logging.info("inHistoAdditionalEff_suffix = {}".format(inHistoAdditionalEff_suffix))
        logging.info("random_latency = {}".format(random_latency))

        if random_latency :
                # for toys (having the "seed<number>_" in their input file name),
                # 'randomly' add a 2-minutes delay
                # this is to optimise the CPU usage on 8-threads systems when running snakemake --cores 4
                if seed % 3 :
                        time.sleep(120)
                        
        start = time.time()
                        
        # initialise the TensorFlow session
	sess = tf.Session(config = session_conf)
        init = tf.global_variables_initializer()
        sess.run(init)

	# set the a few options for the MatrixElement module
        MatrixElement.sess = sess

        # the same for the PhaseSpace module
        PhaseSpace.sess = sess

        # ...and for Utils as well, why not
        Utils.sess = sess

        ########

        # open the file with the additional efficiency factor... if you have to do so
	inFileAdditionalEff = None
        
	if inFileAdditionalEff_name != "" :
		inFileAdditionalEff = TFile.Open(inFileAdditionalEff_name)

		# check that the file exists
                if not inFileAdditionalEff :
	                logging.error("Error: the input file of the additional efficiency does not exist.\n")
			sys.exit(1)

        # application of the efficiency factor:
        # not implemented yet...
        
        ##############
        ## Generate the signal sample
        ##############
        
        # import the resonances from the config file
        sys.path.append(os.path.dirname(configFile_name))
        resonance_module = importlib.import_module(os.path.basename(configFile_name)[:-3])  #-3 removes the '.py' at the end of the string
        
        # get the resonances and the M polarisation from the config file
        A = resonance_module.A
        B = resonance_module.B
        C = resonance_module.C
        M_d1_hel = resonance_module.M_d1_hel

        # get the non-resonant contribution from the config file... if it exists
        non_res = None

        try :
                non_res = resonance_module.non_res
        except :
                pass
        
        logging.info("")
        logging.info("Imported resonances:")
        logging.info("A = {}".format([A_res.name for A_res in A]))
        logging.info("B = {}".format([B_res.name for B_res in B]))
        logging.info("C = {}".format([C_res.name for C_res in C]))
        
        resonances = A + B + C
                     
        switches = Optimisation.Switches(len(resonances) + 1)
        
        # define the phase space
        phase_space = PhaseSpace.ThreeBodyPhaseSpace()
        
        # function to set the matrix element
        def model(datapoint, component_index = -1):
                return MatrixElement.MatrixElement(phase_space, datapoint, switches,
	                                           component_index, resonances = resonances,
                                                   non_res = non_res, M_d1_hel_ = M_d1_hel,
                                                   eff_shape = None)
        
        # build the PDFs
        data_placeholder = phase_space.data_placeholder
        data_pdf = model(data_placeholder)
        
        #############
        
        # generate a uniform sample
        norm_sample = sess.run(phase_space.UniformSample(n_events_norm))
        norm_sample = sess.run(phase_space.Filter(norm_sample))
        
        # start with the fun: estimate the maximum of the density function
	majorant = ToyMC.EstimateMaximum(sess, data_pdf, data_placeholder, norm_sample) * 1.1
        
        # check that the majorant is positive
        if not (majorant > 0):
                logging.error("GenerateToys::Error, maximum = {}\n".format(majorant))
                exit(0)
        
        logging.info("\n")
        logging.info("Running the toy MC")
        
        # do you want a random seed?
        # This would overwrite the already set seed
        if random_seed :
                seed = int(time.clock() * 10000000.)
        else :
                # from mythical past, it's better to use large seeds
                seed = int(seed * 1000.)
                
        logging.info("seed = {}".format(seed))
        
        # run the toy MC!
	data_sample = ToyMC.RunToyMC(sess, data_pdf, data_placeholder, phase_space,
                                     n_events, majorant, chunk = 100,
                                     seed = seed)
        
        #######
        
        logging.info("Saving the toy MC in {}".format(outFile_name))
        
        # save the output
        out_file = TFile.Open(outFile_name, "RECREATE")

        # add some additional variables to the sample
        # that's what I hate of all this pythonic magic, especially if combined with Tensorflow
        for var in other_variables:
                data_sample = numpy.c_[ data_sample, sess.run(eval("phase_space." + var[0] + "(data_sample)"))]

        if sig_weight_name != "" :
                # add a weight (= 1.) which will be used as sweight
                data_sample = numpy.c_[ data_sample, numpy.ones(numpy.size(data_sample, 0)) ]

        out_weight_name = []
        
        if sig_weight_name != "" :
                out_weight_name = [sig_weight_name]
                
        # write the tree of the signal generated sample
	Optimisation.FillNTuple(outTree_name, data_sample,
                                [var[1] for var in variables] + [addvar[1] for addvar in other_variables] + out_weight_name)
        
        ##############
        ## Generate the normalisation sample
        ##############

        # generate a uniform sample
        norm_sample = sess.run(phase_space.UniformSample(n_events_norm))

        # add some additional variables to the sample
        # that's what I hate of all this pythonic magic, especially if combined with Tensorflow
        for var in other_variables:
		norm_sample = numpy.c_[ norm_sample, sess.run(eval("phase_space." + var[0] + "(norm_sample)"))]

        if norm_weight_name != "" :
                # add a weight (= 1.) which will be used as MC-reweighting weight
	        norm_sample = numpy.c_[ norm_sample, numpy.ones(numpy.size(norm_sample, 0)) ]
                
        out_weight_name = []

	if norm_weight_name != "" :
	        out_weight_name = [norm_weight_name]
                
        # write the tree of the normalisation sample
	Optimisation.FillNTuple(outTree_name + "_normalisation", norm_sample,
	                        [var[1] for var in variables] + [addvar[1] for addvar in other_variables] + out_weight_name)
        
        logging.info("Toys saved to {}".format(out_file.GetName()))

        # close the file
        out_file.Close()

        #############
        ## Save out the B_LS couplings
        #############
        
        # print the B_LS couplings of the resonances, for debug
        for res in resonances:
                Utils.print_BLS(res)
                
        # open the output files with the parameters
        outFile_params_cartesian = open(outFile_params_name + "_params_cartesian.info", "w")
        outFile_params_polar = open(outFile_params_name + "_params_polar.info", "w")
        
        # write out the values of the B_LS couplings
        # and the size of the non-resonant contribution
        # in both cartesian and polar coordinates
        #...and the M-d1 helicity factors as well, why not 
        [Utils.SaveBLS_cartesian(res, outFile_params_cartesian) for res in resonances]
        non_res.SaveNonResSize(outFile_params_cartesian)
        Utils.Save_M_d1_helicities(outFile_params_cartesian, MatrixElement.M_d1_hel)
        [Utils.Save_helcouplings(res, outFile_params_cartesian) for res in resonances]
        [Utils.Save_MassAndWidths(res, outFile_params_cartesian) for res in resonances]
        
        [Utils.SaveBLS_polar(res, outFile_params_polar) for res in resonances]
	non_res.SaveNonResSize(outFile_params_polar)
        Utils.Save_M_d1_helicities(outFile_params_polar, MatrixElement.M_d1_hel)
        [Utils.Save_helcouplings(res, outFile_params_polar) for res in resonances]
        [Utils.Save_MassAndWidths(res, outFile_params_polar) for res in resonances]
        
        # close the files
        outFile_params_cartesian.close()
        outFile_params_polar.close()

        #############
        ## compute and write out the generated fit fractions and interference terms
        #############

        # total PDF integral over the normalisation sample
	#pdf_norm = sess.run(data_pdf, feed_dict = {data_placeholder : norm_sample} )
        #total_int = np.sum(pdf_norm)
        
        fit_fractions, total_int = Fractions_Interference.WriteFitFractionsWithErrors(sess, phase_space, data_pdf, data_placeholder,
                                                                                      switches, norm_sample,
                                                                                      resonances, outFile_params_name,
                                                                                      errors = True, results = None)

        # compute the interference terms only if you have more then one resonance
        if len(resonances) > 1 :
                     Fractions_Interference.WriteInterferences(sess, phase_space, total_int, norm_sample,
                                                               resonances, outFile_params_name)
        
        #############

        # print out the total execution time
        end = time.time()
        logging.info("GenerateToys:: Execution time = {} min".format(str((end - start)/60.)))
        
        # close the session
        sess.close()
        
