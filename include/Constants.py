
# Alessio Piucci
# 17/11/2017
# Python module containing some constants... and settings

import sys

#import the local libraries
sys.path.append("../amplitude_fits/")

from Constants import *
import Optimisation
from Interface import Const

# helicities of the (pseudo)scalars, just to avoid to put hard-coded 0 in the code
hel_K = 0
hel_D0 = 0

# some colours used by the plotting macros
kRed = 2
kYellow = 5 
kBlack = 1
kBlue = 4
kGreen = 416
kMagenta = 616
kGray = 920
kBrown = 28
kWhite = 0
kAzure = 860
kPink = 900
kCyan = 432
kSpring = 820
kOrange = 800
kViolet = 880
kTeal = 840
kPurple = 9
kacaso_1 = 30
kacaso_2 = 45
kacaso_3 = 60
kacaso_4 = 75

##############

# class to store the PDG infos of a particle/resonance
class PDGPart():
    def __init__(self, name = "", label = "",
                 tex_name = "", colour = kBlack,
                 mass_float = -999., mass_err_float = -999.,
                 width_float = -999., width_err_float = -999.,
                 spin = -999., parity = -999.):
        
        self.name     = name
        self.label    = label
        self.tex_name = tex_name

        # set the mass and widths both as float and Const variables
        # please, don't ask me why
        self.mass_float      = mass_float
        self.mass_err_float  = mass_err_float
        self.width_float     = width_float
        self.width_err_float = width_err_float
        
        self.mass      = Const(mass_float)
        self.mass_err  = Const(mass_err_float)
        self.width     = Const(width_float)
        self.width_err = Const(width_err_float)
        
        self.spin   = spin
        self.parity = parity

        self.colour = colour
        
##############

# remember that the massese are in GeV/c^2,
# and the spin values are doubled!

# get the masses from pyPDT if it's installed... otherwise use hardcoded values
Lb_mass = 0.
Lc_mass = 0.
D0_mass = 0.
K_mass = 0.

try :
    import pypdt
    Lb_mass = float(pypdt.PDT()[5122].mass)
    Lc_mass = float(pypdt.PDT()[4122].mass)
    D0_mass = float(pypdt.PDT()[421].mass)
    K_mass  = float(pypdt.PDT()[321].mass)
except :
    Lb_mass = 5.61951
    Lc_mass = 2.28646
    D0_mass = 1.86483
    K_mass  = 0.493677
    
Lb = PDGPart(name = "Lb", label = "#Lambda_{b}^{0}",
             tex_name = "\HepProcess{\PLambdab}",
             mass_float = Lb_mass, width_float = -999.,
             spin = 1, parity = +1)

Lc = PDGPart(name = "Lc", label = "#Lambda_{c}^{+}",
             tex_name = "\HepProcess{\PLambdac}",
             mass_float = Lc_mass, width_float = -999.,
             spin = 1, parity = +1)

D0 = PDGPart(name = "D0", label = "D^{0}",
             tex_name = "\HepProcess{\APDzero}",
             mass_float = D0_mass, width_float = -999.,
             spin = 0, parity = -1)

K = PDGPart(name = "K", label = "K^{-}",
            tex_name = "\HepProcess{\PKm}",
            mass_float = K_mass, width_float = -999.,
            spin = 0, parity = -1)

# to specify our particular decay within the PhaseSpace and MatrixElement modules,
# define which are the M --> d1 d2 d3 particles
# If you have a different decay (but with same spin compositions),
# you only have to specify different M, d1, d2 and 3... --> IN PRINCIPLE <--
M = Lb
d1 = Lc
d2 = D0
d3 = K

# helicities that can be taken by mother and daughter particles
M_helicities =  range(-M.spin, M.spin + 1, 2)     # Lb: +-1
d1_helicities = range(-d1.spin, d1.spin + 1, 2)   # Lc: +-1
d2_helicities = range(-d2.spin, d2.spin + 1, 2)   # D0: 0
d3_helicities = range(-d3.spin, d3.spin + 1, 2)   # K:  0

######

# extracted from MC
ratio_Xic2790_Xic2815_widths = 2.1 # 2.4
ratio_Dsstar2700_Dsstar2573_widths = 5.6

######

# list of all possible resonances which might contribute

# "old" Pc states
Pc_4380 = PDGPart(name = "Pc4380", label = "P_{c}(4380)",
                  tex_name = "P^{+}_{c}(4380)",
                  mass_float = 4.380,
                  width_float = 0.205,
                  spin = 3, parity = -1)

Pc_4450 = PDGPart(name = "Pc4450", label = "P_{c}(4450)",
                  tex_name = "P^{+}_{c}(4450)",
                  mass_float = 4.448, width_float = 0.040,
		  spin = 5, parity = +1)

# "new" Pc states
Pc_4312 = PDGPart(name = "Pc4312", label = "P_{c}(4312)",
                  tex_name = "P^{+}_{c}(4312)",
                  mass_float = 4.312, width_float = 0.014,
                  spin = 1, parity = -1)

Pc_4440 = PDGPart(name = "Pc4440", label = "P_{c}(4440)",
                  tex_name = "P^{+}_{c}(4440)",
                  mass_float = 4.450, width_float = 0.069,
                  spin = 1, parity = -1)

Pc_4457 = PDGPart(name = "Pc4457", label = "P_{c}(4457)",
                  tex_name = "P^{+}_{c}(4457)",
                  mass_float = 4.457, width_float = 0.013,
                  spin = 1, parity = -1)

######


Ds0_2317 = PDGPart(name = "Ds0_2317", label = "D_{s0}(2317)",
                   tex_name = "\HepProcess{\PDsstar_0(2317)}",
                   colour = kOrange,
                   mass_float = 2.318, mass_err_float = 0.0006,
                   width_float = 0.0038, width_err_float = 0.,
                   spin = 0, parity = +1)

# not allowed, by parity considerations
Ds1_2460 = PDGPart(name = "Ds1_2460", label = "D_{s1}(2460)",
                   tex_name = "\HepProcess{\PDsstar_1(2460)}",
                   mass_float = 2.460, width_float = 0.0035,
                   spin = 2, parity = +1)

# not allowed, by parity considerations
Ds1_2536 = PDGPart(name = "Ds1_2536", label = "D_{s1}(2536)",
                   tex_name = "\HepProcess{\PDsstar_1(2536)}",
                   mass_float = 2.535, width_float = 0.001,
                   spin = 2, parity = +1)

Ds2_2573 = PDGPart(name = "Ds2_2573", label = "D_{s2}(2573)",
                   tex_name = "\HepProcess{\PDsstar_2(2573)}",
                   colour = kAzure,
		   mass_float = 2.569, mass_err_float = 0.0008,
                   width_float = 0.017, width_err_float = 0.0007,
		   spin = 4, parity = +1)

Ds1_2700 = PDGPart(name = "Ds1_2700", label = "D_{s1}^{*}(2700)",
                   tex_name = "\HepProcess{\PDsstar_1(2700)}",
                   colour = kBrown,
                   mass_float = 2.708, mass_err_float = 0.0036,
                   width_float = 0.120, width_err_float = 0.011,
		   spin = 2, parity = -1)

Ds1_2860 = PDGPart(name = "Ds1_2860", label = "D_{s1}^{*}(2860)",
                   tex_name = "\HepProcess{\PDsstar_1(2860)}",
                   colour = kGreen,
		   mass_float = 2.859, mass_err_float = 0.027,
                   width_float = 0.159, width_err_float = 0.08,
		   spin = 2, parity = -1)

Ds3_2860 = PDGPart(name = "Ds3_2860", label = "D_{s3}^{*}(2860)",
                   tex_name = "\HepProcess{\PDsstar_3(2860)}",
                   colour = kCyan,
		   mass_float = 2.860, mass_err_float = 0.007,
                   width_float = 0.053, width_err_float = 0.01,
		   spin = 6, parity = -1)

Dsj_3040 = PDGPart(name = "DsJ_3040", label = "D_{sJ}(3044)",
                   tex_name = "\HepProcess{\PDs_J(3040)}",
		   mass_float = 3.044, width_float = 0.239,
		   spin = 2, parity = -1)

Xic_2790 = PDGPart(name = "Xic_2790", label = "#Xi_{c}(2790)",
                   tex_name = "\HepProcess{\PXiczero(2790)}",
                   colour = kMagenta,
		   mass_float = 2.793, mass_err_float = 0.0012,
                   width_float = 0.010, width_err_float = 0.0011,
		   spin = 1, parity = -1)

Xic_2815 = PDGPart(name = "Xic_2815", label = "#Xi_{c}(2815)",
                   tex_name = "\HepProcess{\PXiczero(2815)}",
                   colour = kGreen,
		   mass_float = 2.820, mass_err_float = 0.00032,
                   width_float = 0.002, width_err_float = 0.00025,
		   spin = 3, parity = -1)

# from LHCb-ANA-2019-010
Xic_2920 = PDGPart(name = "Xic_2920", label = "#Xi_{c}(2920)",
                   tex_name = "\HepProcess{\PXiczero(2920)}",
                   colour = kacaso_1,
                   mass_float = 2.92326, mass_err_float = 0.00031,
                   width_float = 0.00567, width_err_float = 0.00172,
                   spin = -999, parity = -999)

# from LHCb-ANA-2019-010
Xic_2940 = PDGPart(name = "Xic_2940", label = "#Xi_{c}(2940)",
                   tex_name = "\HepProcess{\PXiczero(2940)}",
                   colour = kacaso_2,
                   mass_float = 2.93851, mass_err_float = 0.00030,
                   width_float = 0.01025, width_err_float = 0.00172,
                   spin = -999, parity = -999)

# only one PDG star! Buuuuh
# observed by Belle in B --> Lc Lc K decays
Xic_2930 = PDGPart(name = "Xic_2930", label = "#Xi_{c}(2930)",
                   tex_name = "\HepProcess{\PXiczero(2930)}",
		   mass_float = 2.931, width_float = 0.036,
		   spin = -999, parity = -999)


Xic_2970 = PDGPart(name = "Xic_2970", label = "#Xi_{c}(2970)",
                   tex_name = "\HepProcess{\PXiczero(2970)}",
                   colour = kGray,
                   # from the PDG
                   #mass_float = 2.968, mass_err_float = 0.0008,
                   #width_float = 0.028, width_err_float = 0.0037,
                   #
                   # from LHCb-ANA-2019-010
                   mass_float = 2.9649, mass_err_float = 0.00032,
                   width_float = 0.01369, width_err_float = 0.00124,
		   spin = -999, parity = -999)

Xic_3055 = PDGPart(name = "Xic_3055", label = "#Xi_{c}(3055)",
                   tex_name = "\HepProcess{\PXiczero(3055)}",
                   colour = kacaso_3,
                   mass_float = 3.056,
                   width_float = 0.008,
                   spin = -999, parity = -999)

Xic_3080 = PDGPart(name = "Xic_3080", label = "#Xi_{c}(3080)",
                   tex_name = "\HepProcess{\PXiczero(3080)}",
                   colour = kacaso_4,
		   mass_float = 3.080, mass_err_float = 0.00014,
                   width_float = 0.006, width_err_float = 0.0022,
                   spin = -999, parity = -999)

###############

# used by the Flatte' distributions
Ds = PDGPart(name = "Ds", label = "D_{s}",
             tex_name = "\HepProcess{\PDsplus}",
             mass_float = 1.968, width_float = -999.,
             spin = 0, parity = -1)

pi0 = PDGPart(name = "pi0", label = "#pi^{0}",
              tex_name = "\HepProcess{\Ppizero}",
              mass_float = 0.145, width_float = -999.,
              spin = 0, parity = -1)

eta = PDGPart(name = "eta", label = "#eta",
              tex_name = "\HepProcess{\Peta}",
              mass_float = 0.547, width_float = -999.,
              spin = 0, parity = -1)

###############

# used by the Kmatrix
Dstar_2007 = PDGPart(name = "Dstar_2007", label = "D^{*}(2007)",
                     tex_name = "D^*(2007)",
                     mass_float = 2.00685, width_float = -999.,
                     spin = 2, parity = -1)

D1_2420 = PDGPart(name = "D1_2420", label = "D_{1}(2420)",
                  tex_name = "D_1^0(2420)",
                  mass_float = 2.4208, width_float = -999.,
                  spin = 2, parity = +1)

###############

# hidden-charm Pc predictions from arXiv:1703.03885 

P_s11 = PDGPart(name = "Pc_s11", label = "P_{c,s11}", tex_name = "Pc_{s11}",
                mass_float = 4.295, width_float = 0.0074,
                spin = 1, parity = -1,
                colour = kacaso_1)

P_d11 = PDGPart(name = "Pc_d11", label = "P_{c,d11}", tex_name = "Pc_{d11}",
                mass_float = 4.334, width_float = 0.056,
                spin = 1, parity = 1,
                colour = kacaso_2)

P_d13 = PDGPart(name = "Pc_d13", label = "P_{c,d13}", tex_name = "Pc_{d13}",
                mass_float = 4.395, width_float = 0.108,
                spin = 3, parity = -1,
                colour = kGray)
