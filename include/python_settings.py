
# Generic Python settings of the project
# Alessio Piucci, 10/04/2018

# to disable the GPU
import os
os.environ["CUDA_VISIBLE_DEVICES"] = ""

# limit on the number of Minuit calls
Minuit_call_limit = 500000

# import the logging configuration
import logging
from logging.config import fileConfig
#logging.basicConfig(stream = sys.stderr, level = logging.DEBUG)
logging.config.fileConfig('../include/logging.ini')  # I have to specify the path in this way because the instruction is called by ../amplitude_fits/blabla.py scripts
