
# Collection of useful methods
# Alessio Piucci

# Tensorflow stuff
import Optimisation
import Interface

# import the Python settings
from python_settings import *

from Constants import *

# ROOT stuff
from ROOT import TFile, TH1F, TCanvas, TPad, TH1D, TLegend, TLine, gStyle

import tensorflow as tf
import math

# some global variables
sess = None

############

# a little bit of linear algebra, why not!

# transfose the matrix
def transposeMatrix(m):
    
    return map(list,zip(*m))

# get the matrix minor
def getMatrixMinor(m, i, j):

    return [row[:j] + row[j+1:] for row in (m[:i]+m[i+1:])]

# get the matrix determinant
def getMatrixDeterminant(m):

    determinant = 0
    
    if len(m) == 2:
        # 2x2 matrix
        determinant = m[0][0]*m[1][1]-m[0][1]*m[1][0]
    elif len(m) == 3:
        # 3x3 matrix
        determinant = m[0][0]*m[1][1]*m[2][2] + m[0][1]*m[1][2]*m[2][0] + m[0][2]*m[1][0]*m[2][1] - m[0][2]*m[1][1]*m[2][0] - m[0][1]*m[1][0]*m[2][2] - m[0][0]*m[1][2]*m[2][1] 
    else :
        for c in range(len(m)):
            determinant += ((-1)**c)*m[0][c] * getMatrixDeterminant(getMatrixMinor(m, 0, c))

    # check that the matrix is invertible
    if determinant == 0 :
        logging.error("getMatrixDeterminant: Error: that's embarrassing, the D matrix is not invertible...")
        exit(1)

    return determinant

# get the matrix inverse
def getMatrixInverse(m):

    # get the determinant
    determinant = getMatrixDeterminant(m)

    # special case for 2x2 matrix
    if len(m) == 2:

        return [[m[1][1]/determinant, -1*m[0][1]/determinant],
                [-1*m[1][0]/determinant, m[0][0]/determinant]]

    if len(m) == 3:
        cof00 =  (m[1][1]*m[2][2] - m[2][1]*m[1][2])
        cof01 = -(m[1][0]*m[2][2] - m[2][0]*m[1][2])
        cof02 =  (m[1][0]*m[2][1] - m[2][0]*m[1][1])

        cof10 = -(m[0][1]*m[2][2] - m[2][1]*m[0][2])
        cof11 =  (m[0][0]*m[2][2] - m[2][0]*m[0][2])
        cof12 = -(m[0][0]*m[2][1] - m[2][0]*m[0][1])

        cof20 =  (m[0][1]*m[1][2] - m[1][1]*m[0][2])
        cof21 = -(m[0][0]*m[1][2] - m[1][0]*m[0][2])
        cof22 =  (m[0][0]*m[1][1] - m[1][0]*m[0][1])
        
        return [cof00/determinant, cof10/determinant, cof20/determinant,
                cof01/determinant, cof11/determinant, cof21/determinant,
                cof02/determinant, cof12/determinant, cof22/determinant]
        
    # find the matrix of cofactors
    cofactors = []

    for r in range(len(m)):

        cofactorRow = []

        for c in range(len(m)):
            minor = getMatrixMinor(m, r, c)
            cofactorRow.append(((-1)**(r+c)) * getMatrixDeternminant(minor))

        cofactors.append(cofactorRow)

    cofactors = transposeMatrix(cofactors)

    for r in range(len(cofactors)):
        for c in range(len(cofactors)):
            cofactors[r][c] = cofactors[r][c]/determinant

    return cofactors

############

def GetL(spin, parity) :
    l1 = (spin-1)/2     # Lowest possible momentum
    p1 = 2*(l1 % 2)-1   # P=(-1)^(L1+1), e.g. P=-1 if L=0
    
    if p1 == parity : return l1
    
    return l1+1

def GetSvalues(JB, JC):
    """
      Return a set of discrete spin values in |JB - JC| <= S <= JB + JC
      This assumes you're working with doubled angular momentum numbers
    """
    
    return range(abs(JB - JC), JB + JC + 1, 2)


def SanN(n):
    """
      Turn a signed integer into an alphanumeric string +1 -> "p1", -3 -> "m3"
    """
    
    if n < 0:
        return "m" + str(-n)
    elif n > 0:
        return "p" + str(n)
    else:
        return "0"

# get the L orbital angular momenta between the resonance
# and the spectator particle
def GetLvalues_res_spectator(J_res, J_spectator):
    
    L_values = []
    
    # get the S values of the resonance + spectator
    S_values = GetSvalues(J_res, J_spectator)
    
    # loop over the allowed S values of the resonance + spectator combination
    for S in S_values:
        
        # loop over the allowed L = J_M + S of the M decay
        for L in range(abs(M.spin - S), M.spin + S + 1, 2):
            
            # if the parity is conserved in the decay,
            # check that the current L is allowed                                                                                                                                      
            # but the M decays weakly, so I'm sorry but no P conservation
            #if parity_conserved :
            #        if (P_B * P_C * (-1)**(L/2)) != P_A:
            #                continue
            
            L_values += [L]
            
    return L_values

######################

# print the B_LS couplings, for debug purposes
def print_BLS(res) :

    # loop over the BLS of the M and resonance decays
    for BLS in [res.BLS_M, res.BLS_res] :
        
        # loop over the L and S values
        for S in BLS.keys():
            for L in BLS[S].keys():
            
                # print the values                                                                                                                                                 
                logging.info("print_BLS: {} = {}".format(BLS[S][L].name, sess.run(BLS[S][L].value)))
                
    return

# filter the allowed couplings
def filter_allowed_couplings(B_SL, allowedL):
    
    # loop over the S, L
    for S in B_SL.keys():
        for L in B_SL[S].keys():
            
            # only accept the B_SL couplings with the allowed L values                                                                                                                 
            # delete the the ones not allowed                                                                                                                                          
            if not L in allowedL:
		logging.debug("filter_allowed_couplings: Deleting B_SL[L = {}, S = {}]".format(S, L))
                
                del B_SL[S][L]
                
                continue
            
    return B_SL

# save all the BLS couplings to an output file,
# in cartesian coordinates
def SaveBLS_cartesian(resonance, outFile) :
    
    # print the B_LS couplings of the M decay
    for S in resonance.BLS_M.keys():
        for L in resonance.BLS_M[S].keys():
            
            # get the coupling name
            name = resonance.BLS_M[S][L].name
            
            # write the magnitude
            outFile.write(name + "_Real\t" + str(sess.run(Interface.Real(resonance.BLS_M[S][L].value))) + "\n")
            
            # write the phase                                                                                                                                                  
            outFile.write(name + "_Imag\t" + str(sess.run(Interface.Imaginary(resonance.BLS_M[S][L].value))) + "\n")
                
    # print the B_LS couplings of the resonance decay
    for S in resonance.BLS_res.keys():
        for L in resonance.BLS_res[S].keys():
            
            name = resonance.BLS_res[S][L].name
            
            # write the magnitude                                                                                                                                              
            outFile.write(name + "_Real\t" + str(sess.run(Interface.Real(resonance.BLS_res[S][L].value))) + "\n")
            
            # write the phase
            outFile.write(name + "_Imag\t" + str(sess.run(Interface.Imaginary(resonance.BLS_res[S][L].value))) + "\n")
            
    return

# save all the BLS couplings to an output file,
# in polar coordinates
def SaveBLS_polar(resonance, outFile) :

    # print the B_LS couplings of the M decay
    for S in resonance.BLS_M.keys():
        for L in resonance.BLS_M[S].keys():
            
            name = resonance.BLS_M[S][L].name
            
            # write the magnitude                                                                                                                                              
            outFile.write(name + "_Mag\t" + str(sess.run(Interface.Mag(resonance.BLS_M[S][L].value))) + "\n")
            
            # write the phase
            outFile.write(name + "_Phase\t" + str(sess.run(Interface.Phase(resonance.BLS_M[S][L].value))) + "\n")
                
    # print the B_LS couplings of the resonance decay
    for S in resonance.BLS_res.keys():
        for L in resonance.BLS_res[S].keys():

            name = resonance.BLS_res[S][L].name
            
            # write the magnitude                                                                                                                                              
            outFile.write(name + "_Mag\t" + str(sess.run(Interface.Mag(resonance.BLS_res[S][L].value))) + "\n")
            
            # write the phase
            outFile.write(name + "_Phase\t" + str(sess.run(Interface.Phase(resonance.BLS_res[S][L].value))) + "\n")
                
    return

# save all the parameters of the amplitude model:
# couplings, non-resonant size, mass and widths
# fitted or fixed, doesn't matter: save EVERYTHING!
# For consistency with the other file formats, put (unvalid) uncertainties
def SaveAllParams(outFile_name, resonances = [], nonres = None) :
    
    # open the output file
    outFile = open(outFile_name, "w")
    
    # loop over the resonances
    for res in resonances :
        
        #############
        # save the couplings of the M and resonance decays
        #############
        
        # loop over the set of M and resonance decays couplings
        for BLS in [res.BLS_M, res.BLS_res] :
            
            # loop over the L and S values
            for S in BLS.keys():
                for L in BLS[S].keys():
                    
                    # get the name of the coupling components
                    comp_1_name = BLS[S][L].comp_1_name
                    comp_2_name = BLS[S][L].comp_2_name
                    
                    # get the values of the couplings components
                    value_1 = BLS[S][L].comp_1
                    value_2 = BLS[S][L].comp_2
                    
                    # save the components
                    outFile.write(comp_1_name + "\t" + str(sess.run(BLS[S][L].comp_1)) + "\t-999.\n")
                    outFile.write(comp_2_name + "\t" + str(sess.run(BLS[S][L].comp_2)) + "\t-999.\n")
                    
        #############
        # save the mass and width(s) of the resonance line shape
        #############
        
        if "Polynomial" in res.res_type :
            for power, coeff in enumerate(res.pol_coefficients) :
                outFile.write(res.name + "__x" + str(power) + "\t" + str(sess.run(coeff)) + "\t-999.\n")
        else :
            # save the mass
            outFile.write(res.name + "__mass" + "\t" + str(sess.run(res.mass)) + "\t-999.\n")
            
            # save the width(s)
            if "Flatte" in res.res_type :
                # this is the case for the Flatte' line shape  
                outFile.write(res.name + "__width" + "\t" + str(sess.run(res.width[0])) + "\t-999.\n")
                outFile.write(res.name + "__width_2" + "\t" + str(sess.run(res.width[1])) + "\t-999.\n")
            else :
                # this is the standard case
                outFile.write(res.name + "__width" + "\t" + str(sess.run(res.width)) + "\t-999.\n")

        #############
        # save the K-matrix parameters
        #############

        if "KMatrix" in res.res_type :

            # number of channels
            outFile.write(res.name + "__Kmat__num_channels" + "\t" + str(res.Kmat__num_channels) + "\t-999\n")

            # channel used to get the amplitude
            outFile.write(res.name + "__Kmat__channel" + "\t" + str(res.Kmat__channel) + "\t-999.\n")

            # resonance masses
            for i, mass in enumerate(res.Kmat__resonance_masses) :

                resonance_mass = 0

                # let's see if the current mass is a tensor or just a complex number
                try :
                    resonance_mass = sess.run(mass)
                except :
                    resonance_mass = mass

                # the resonance mass is a complex, in this formalism
                outFile.write(res.name + "__Kmat__resonance_masses_" + str(i) + "_Real\t" + str(resonance_mass.real) + "\t-999.\n")
                outFile.write(res.name + "__Kmat__resonance_masses_" + str(i) + "_Imag\t" + str(resonance_mass.imag) + "\t-999.\n")
                
            # alpha couplings
            for i, coupl in enumerate(res.Kmat__alpha_couplings) :

                coupling = 0
                             
                # let's see if the current coupling is a tensor or just a complex number
                try :
                    coupling = sess.run(coupl[0])
                except :
                    coupling = coupl[0]
                
                outFile.write(res.name + "__Kmat__alpha_couplings_" + str(i) + "_Real\t" + str(coupling.real) + "\t-999.\n")
                outFile.write(res.name + "__Kmat__alpha_couplings_" + str(i) + "_Imag\t" + str(coupling.imag) + "\t-999.\n")
                
            # g couplings
            for i, i_coupl in enumerate(res.Kmat__g_couplings) :
                for j, coupl in enumerate(i_coupl) :
                    
                    coupling = 0
                    
                    # let's see if the current coupling is a tensor or just a complex number
                    try :
                        coupling = sess.run(coupl)
                    except :
                        coupling = coupl
                    
                    outFile.write(res.name + "__Kmat__g_couplings_" + str(i) + "_" + str(j) + "_Real\t" + str(coupling.real) + "\t-999.\n")
                    outFile.write(res.name + "__Kmat__g_couplings_" + str(i) + "_" + str(j) + "_Imag\t" + str(coupling.imag) + "\t-999.\n")

            # widths factors of the sigma vector
            for i, width in enumerate(res.Kmat__width_factors) :

                width_value = 0

                # let's see if the current width is just a float or a tensor
                try :
                    width_value = sess.run(width)
                except :
                    width_value = width
                
		outFile.write(res.name + "__Kmat__width_factors_" + str(i) + "\t" + str(width_value) + "\t-999.\n")
                
            # daughter masses
	    for i, mass in enumerate(res.Kmat__daughters_masses) :

                # two daughters per channel, with complex masses = 4 daughters mass parameters per channel
                outFile.write(res.name + "__Kmat__daughters_masses_0_" + str(i) + "_Real\t" + str(mass[0].real) + "\t-999.\n")
                outFile.write(res.name + "__Kmat__daughters_masses_0_" + str(i) + "_Imag\t" + str(mass[0].imag) + "\t-999.\n")
                outFile.write(res.name + "__Kmat__daughters_masses_1_" + str(i) + "_Real\t" + str(mass[1].real) + "\t-999.\n")
		outFile.write(res.name + "__Kmat__daughters_masses_1_" + str(i) + "_Imag\t" + str(mass[1].imag) + "\t-999.\n")
                
            # orbital momenta of the decays
            for i, L in enumerate(res.Kmat__L_channels) :
                outFile.write(res.name + "__Kmat__L_channels_" + str(i) + "\t" + str(L) + "\t-999.\n")

            # background terms
            for i, backg in enumerate(res.Kmat__background_terms) :

                background_term = 0
                
                # just to be sure if it's a sensor or a double
                try :
                    background_term = sess.run(backg)
                except :
                    background_term = backg

                outFile.write(res.name + "__Kmat__background_terms_" + str(i) + "\t" + str(background_term) + "\t-999.\n")
                
    #############
    # save the non-resonant size
    #############
    
    if nonres != None :
        nonres.SaveNonResSize(outFile)
            
    return

#######################

# to parse operations with fit parameters
# the input string must be in the format:
# equiv%'<param_name>' to assign to the parameter a value from another param
# mult%<double>%'<param_name>'% for multiplication: Const(<double>) * param
# div%<double>%'<param_name>'% for division: Const(<double>) / param
# add%<double>%'<param_name_1>:<param_name_2>:...'%  for sum: Const(<double>) + param_1 + param_2 + ... 
# min%<double>%'<param_name_1>:<param_name_2>:...'%  for subtraction: Const(<double>) - param_1 - param_2 - ...
def ParseOpParameters(opparam_string) :

    # first check if the input argument is a string or a parameter/Const
    if not isinstance(opparam_string, basestring):
        return opparam_string
    
    operation = None
    const = None
    param_name = None
    params = []
    
    # if "equiv" in in the input string,
    # then the <double> of the operation will not be present 
    if "equiv" in opparam_string :
        operation, param_name = opparam_string.split("%")
    else :
        operation, const, param_name = opparam_string.split("%")
        
        # convert the const from string to a double
        const = float(const)

    # put the parameters (or the only one) in a array
    param_names = param_name.split(":")

    logging.info("Utils::ParseOpParameters: param_names = {}".format(param_names))
    
    # get the parameters by name, looping over the fit parameters
    for par_name in param_names :

        param_found = False
        
        for param_tf in tf.trainable_variables() :

            logging.debug("Utils::ParseOpParameters: parameter = {}".format(param_tf.par_name))
            
            if param_tf.par_name == par_name :
                params.append(param_tf)
                param_found = True

        if param_found == False :
            logging.error("Utils::ParseOpParameters: Error, the parameter {} has not been found".format(par_name))
            exit(1)
            
    logging.debug("Utils::ParseOpParameters: params = {}".format(params))
    
    # one consistency check: if 'equiv' or 'div' operations are selected,
    # only one parameter can be handled
    if ((operation == "equiv") or (operation == "div")) and (len(params) > 1):
        logging.error(("Utils::ParseOpParameters: Error, {} is selected"
                       + " but there are more than one ({}) parameters involved").format(operation, len(params)))
    
    # now return the operation
    if operation == "equiv" :
        return params[0]
    elif operation == "div" :
        return Const(const) / params[0]
    else :

        partial_result = Const(const)
        
        # loop over the parameters, and perform the operations
        if operation == "mult" :
            for par in params :
                partial_result *= par
        elif operation == "add" :
            for	par in params :
                partial_result += par
        elif operation == "min" :
            for	par in params :
                partial_result -= par
        else :
            # operation not recognised, raise an error
            logging.error("Utils::ParseOpParameters: Error, operation not recognised.")
            exit(1)

        return partial_result

# save the M and d1 helicity factors in an external file
def Save_M_d1_helicities(outFile, M_d1_hel) :

    if M_d1_hel is None :
        
        return
        
    # loop over the M and d1 helicities
    for M_hel in M_d1_hel.keys():
        for d1_hel in M_d1_hel[M_hel].keys():
            
            # write the line to the output file
            outFile.write("Mhel_" + str(M_hel) + "_d1hel_" + str(d1_hel) + "\t" + str(sess.run(M_d1_hel[M_hel][d1_hel])) + "\n")

    return

# save the resonance helicity couplings
def Save_helcouplings(res, outFile) :

    # loop over the helicity couplings
    for hel in res.res_helicity_factors.keys() :
        outFile.write(res.name + "__helcoupling_" + str(hel) + "\t" + str(sess.run(res.res_helicity_factors[hel])) + "\n")
        
    return

# save the mass and widths of a resonance
def Save_MassAndWidths(res, outFile) :

    # no mass neither widths for the polynomial line shape
    if "Polynomial" in res.res_type :
        return
    
    outFile.write(res.name + "__mass\t" + str(sess.run(res.mass)) + "\n")
    
    # two widths for the Flatte line shape, or one for the others
    if "Flatte" in res.res_type :
        outFile.write(res.name + "__width\t" + str(sess.run(res.width[0])) + "\n")
        outFile.write(res.name + "__width_2\t" + str(sess.run(res.width[1])) + "\n")
    else :
	outFile.write(res.name + "__width\t" + str(sess.run(res.width)) + "\n")

    return

#######################

# import the fit parameters (couplings, non-resonant size, mass and widths) from a text file
def ImportParameters(inFile_name) :

    # open the input file
    inFile = open(inFile_name, "r")

    dict_params = {}
    
    # loop over the file lines
    for line in inFile :
        
        # split the line
        split_line = line.split()
        
        # add a new element to the dictionary: the key is the coupling name
        dict_params[split_line[0]] = Const(float(split_line[1]))

    logging.info("Imported params = {}".format(sess.run(dict_params)))
    
    return dict_params

# set the resonance couplings from a dictionary
def SetCouplings_fromdict(res, parameters) :

    # loop over the set of M and res decays couplings                                                                                                                                        
    for BLS in [res.BLS_M, res.BLS_res] :
        
        # loop over the L and S values
        for S in BLS.keys():
            for L in BLS[S].keys():
                    
                    # get the coupling name
                    name = BLS[S][L].name
                    
                    # get the name of the coupling components
                    comp_1_name = BLS[S][L].comp_1_name
                    comp_2_name = BLS[S][L].comp_2_name
                    
                    # retrieve the components from the dictionary,
                    # and set them
                    value_1 = parameters[comp_1_name]
                    value_2 = parameters[comp_2_name]
                    
                    BLS[S][L].comp_1 = value_1
                    BLS[S][L].comp_2 = value_2

                    # update the value of the coupling
                    BLS[S][L].UpdateValue()
                    
                    logging.debug(("SetCouplings_fromdict::comp_1_name = {}, value_1 = {}, comp_2_name = {},"
                                   + "value_2 = {}").format(comp_1_name, value_1, comp_2_name, value_2))

    return res

# set the non-resonant size from a dictionary
def SetNonRes_fromdict(non_res, parameters) :

    # overwrite the non-resonant size
    # with the value from the dictionary
    non_res.nonres_size = parameters["nonres_size"]

    logging.debug("SetNonRes_fromdict::non_res.nonres_size = {}".format(non_res.nonres_size))
    
    return non_res

# set the mass and width of the resonance line shape, from a dictionary
def SetMassAndWidth_fromdict(res, parameters) :

    # build the mass name
    mass_name = res.name + "__mass"

    # build the width name
    width_name = res.name + "__width"

    # replace the mass and width values
    # with the ones from the dictionary
    res.mass = parameters[mass_name]

    try :
        # this is the Flatte' case, when you have 2 widths
        res.width = [parameters[width_name], parameters[width_name + "_2"]]
    except :
        # this is the standard case, when you have only one width
        res.width = parameters[width_name]

    logging.debug("SetMassAndWidth_fromdict::res.mass = {}, res.width = {}".format(res.mass, res.width))
    
    return

# update the amplitude model with the parameters taken from an external file:
# couplings, non-resonant size, masses and widths of the resonances  
def UpdateModel(resonances, non_res, inFile_params) :

    # import the parameters: couplings, non resonant size, masses and widths
    parameters = ImportParameters(inFile_params)
    
    # loop over the resonances
    for res in resonances :

        # that's a cumbersome way to update the actual resonances
        # the pass-by-reference a-la-C it's an old memory 
        # thanks for nothing, Python
        
	# update the couplings from the fitted values
        res = SetCouplings_fromdict(res = res, parameters = parameters)
        
        # update the mass and widts from the fitted values
        res = SetMassAndWidth_fromdict(res = res, parameters = parameters)

    # update the non-resonant contribution from the fitted values
    non_res = SetNonRes_fromdict(non_res = non_res, parameters = parameters)

    return resonances, non_res

# dump the values of the helicity amplitudes to a text file
def DumpHelicityAmplitude(resonances, outFile_name) :

    # open the output file
    outFile = open(outFile_name, "w")

    for res in resonances :

        if (res.species == "A-type") or (res.species == "C-type") :
            
            # loop over the resonance helicities,
            # to get the Lb helicity amplitudes
            for hel_res in res.BLSHelAmp_M.keys() :

                # build the line
                line = res.name + "_HelAmp_M_" + str(hel_res) + "\t" + str(sess.run(res.BLSHelAmp_M[hel_res].getvalue()))

                logging.info("{}".format(line))
                
                outFile.write(line + "\n")

            # loop over the helicities of the resonance daughter particle,
            # to get the resonance helicity amplitude
            for hel_daughter in res.BLSHelAmp_res.keys() :

                # build the line
                line = res.name + "_HelAmp_res_" + str(hel_daughter) + "\t" + str(sess.run(res.BLSHelAmp_res[hel_daughter].getvalue()))
                
                logging.info("{}".format(line))
                
                outFile.write(line + "\n")
                
        elif res.species == "B-type" :
            
            # loop over the resonance helicities
            for hel_res in res.BLSHelAmp_M.keys() :
                
                # loop over the d1 helicities in the B frame
                for hel_d1_B in res.BLSHelAmp_M[hel_res].keys() :

                    # build the line
                    line = res.name + "_HelAmp_M_" + str(hel_res) + "_" + str(hel_d1_B) + "\t" + str(sess.run(res.BLSHelAmp_M[hel_res][hel_d1_B].getvalue()))
                    
                    logging.info("{}".format(line))
                    
                    outFile.write(line + "\n")
                    
            # the resonance helicity amplitude for the B resonances
            # is "just" a complex number

            # build the line
            line = res.name + "_HelAmp_res" + "\t" + str(sess.run(res.BLSHelAmp_res.getvalue()))
            
            logging.info("{}".format(line))
            
            outFile.write(line + "\n")
    
    return
    
# dump the parameters of a resonance to a text file
def DumpResonance(res, outFile, for_replacing = False) :

    # resonance name and label
    outFile.write(("\tMatrixElement." + res.res_type + "(name = '" + res.name + "', label = '"
                   + res.label + "', tex_name = '" + res.tex_name+"',\n"))

    # resonance mass
    if (type(res.mass) is Optimisation.FitParameter) and for_replacing :
        outFile.write("\t\tmass = Const(" + res.mass.par_name + "_VALUE),")
    else :
        outFile.write("\t\tmass = Const(" + str(sess.run(res.mass)) + "),")

    # two widths for the Flatte line shape, or one for the others
    if "Flatte" in res.res_type :
        if (type(res.width[0]) is Optimisation.FitParameter) and for_replacing :
            outFile.write("width = [Const(" + res.width[0].par_name + "_VALUE),")
        else :
            outFile.write("width = [Const(" + str(sess.run(res.width[0])) + "),")

        if (type(res.width[1]) is Optimisation.FitParameter) and for_replacing :
            outFile.write(" Const(" + res.width[1].par_name + "_VALUE)],\n")
        else :
            outFile.write(" Const(" + str(sess.run(res.width[1])) + ")],\n")
    else :
        if (type(res.width) is Optimisation.FitParameter) and for_replacing :
            outFile.write("width = Const(" + res.width.par_name + "_VALUE),\n")
        else :
            outFile.write("width = Const(" + str(sess.run(res.width)) + "),\n")
            
    # parameters of the polynomial
    if "Polynomial" in res.res_type :

        outFile.write("\t\tpol_coefficients = [\n")
        
        # loop over the coefficients
        for power, coeff in enumerate(res.pol_coefficients) :

            if (type(coeff) is Optimisation.FitParameter) and for_replacing :
                outFile.write("\t\tConst(x" + str(power) + "_VALUE),\n")
            else :
                outFile.write("\t\tConst(" + str(sess.run(coeff)) + "),\n")

        outFile.write("\t\t],\n")
        
    outFile.write("\t\tspin = " + str(res.spin) + ", parity = " + str(res.parity) + ",\n")
    outFile.write("\t\taboveLmin_M = " + str(res.aboveLmin_M) + ",\n")

    # for the Flatte' line shape
    if res.ma2 == None :
        outFile.write("\t\tma2 = None,\n")
    else :
        outFile.write("\t\tma2 = Const(" + str(sess.run(res.ma2)) + "),\n")

    if res.mb2 == None :
        outFile.write("\t\tmb2 = None,\n")
    else :
        outFile.write("\t\tmb2 = Const(" + str(sess.run(res.mb2)) + "),\n")
        
    # Lb heliciy amplitude to fix?
    if res.fix_Lb_hel_amplitude != None :
        outFile.write("\t\tfix_Lb_hel_amplitude = ('" + res.fix_Lb_hel_amplitude[0] + "', CastComplex(" + str(sess.run(Interface.Abs(res.fix_Lb_hel_amplitude[1]))) + ")),\n")
        
    # heliciy amplitudes to fix?
    if res.fix_all_hel_amplitudes != None :
        outFile.write("\t\tfix_all_hel_amplitudes = CastComplex(" + str(sess.run(Interface.Abs(res.fix_all_hel_amplitudes))) + "),\n")

    # res heliciy amplitude (of the B resonance!) to fix?                                                                                                                          
    if (res.species == "B-type") and (res.fix_Bres_hel_amplitude != None) :
        outFile.write("\t\tfix_Bres_hel_amplitude = CastComplex(" + str(sess.run(Interface.Abs(res.fix_Bres_hel_amplitude))) + "),\n")
                
    # use the BLS representation of the couplings?
    outFile.write("\t\tuse_BLS = " + str(res.use_BLS) + ",\n")

    if res.use_BLS :

        # BLS representation of the couplings
        
        # write the resonance helicity factors
        outFile.write("\t\tres_helicity_factors_reduced = " + str(res.res_helicity_factors_reduced) + ",\n")
        outFile.write("\t\tres_helicity_factors_dict = {\n")

        if res.res_helicity_factors_reduced :
            # loop over the helicity couplings
            for hel_res in res.res_helicity_factors.keys() :
                if (type(res.res_helicity_factors[hel_res]) is Optimisation.FitParameter) and for_replacing :
                    outFile.write("\t\t\t'" + res.name + "__helcoupling_" + str(hel_res) + "' : Const(" + res.res_helicity_factors[hel_res].par_name + "_VALUE),\n")
                else :
                    outFile.write("\t\t\t'" + res.name + "__helcoupling_" + str(hel_res) + "' : Const(" + str(sess.run(res.res_helicity_factors[hel_res])) + "),\n")
        else :
            logging.error("Utils::DumpResonance: Dumping of the full helicity_factors model not implemented yet.")
            
        outFile.write("\t\t},\n")

        # MagAndPhase convention?
        if res.couplings_MagAndPhase :
            outFile.write("\t\tcouplings_RealAndImag = False,\n")
            outFile.write("\t\tcouplings_MagAndPhase = True,\n")
        
        # write the coupling of the M decay
        outFile.write("\t\tBLS_couplings_dict_M = {\n")
        
        # loop over the L and S values
        for S in res.BLS_M.keys():
            for L in res.BLS_M[S].keys():
                if (type(res.BLS_M[S][L].comp_1) is Optimisation.FitParameter) and for_replacing :
                    outFile.write("\t\t\t'" + res.BLS_M[S][L].comp_1_name + "' : Const(" + res.BLS_M[S][L].comp_1_name + "_VALUE),\n")
                else :
                    outFile.write("\t\t\t'" + res.BLS_M[S][L].comp_1_name + "' : Const(" + str(sess.run(res.BLS_M[S][L].comp_1)) + "),\n")

                if (type(res.BLS_M[S][L].comp_2) is Optimisation.FitParameter) and for_replacing :
                    outFile.write("\t\t\t'" + res.BLS_M[S][L].comp_2_name + "' : Const(" + res.BLS_M[S][L].comp_2_name + "_VALUE),\n")
                else :
                    outFile.write("\t\t\t'" + res.BLS_M[S][L].comp_2_name + "' : Const(" + str(sess.run(res.BLS_M[S][L].comp_2)) + "),\n")
                
        outFile.write("\t\t},\n")

        # IF! this is a B resonance and the resonance helicity amplitude is fixed by hand:
        # there are no BLS res couplings to dump

        if not ((res.species == "B-type") and (res.fix_Bres_hel_amplitude != None)) :
            
            # write the coupling of the res decay
            outFile.write("\t\tBLS_couplings_dict_res = {\n")
            
            # loop over the L and S values
            for S in res.BLS_res.keys():
                for L in res.BLS_res[S].keys():
                    
                    if (type(res.BLS_res[S][L].comp_1) is Optimisation.FitParameter) and for_replacing :
                        outFile.write("\t\t\t'" + res.BLS_res[S][L].comp_1_name + "' : Const(" + res.BLS_res[S][L].comp_1_name + "_VALUE),\n")
                    else :
                        outFile.write("\t\t\t'" + res.BLS_res[S][L].comp_1_name + "' : Const(" + str(sess.run(res.BLS_res[S][L].comp_1)) + "),\n")
                        
                    if (type(res.BLS_res[S][L].comp_2) is Optimisation.FitParameter) and for_replacing :
                        outFile.write("\t\t\t'" + res.BLS_res[S][L].comp_2_name + "' : Const(" + res.BLS_res[S][L].comp_2_name + "_VALUE),\n")
                    else :
                        outFile.write("\t\t\t'" + res.BLS_res[S][L].comp_2_name + "' : Const(" + str(sess.run(res.BLS_res[S][L].comp_2)) + "),\n")
                
            outFile.write("\t\t},\n")

        #############
        # save the K-matrix parameters
        #############

        if "KMatrix" in res.res_type :

            # number of channels
            outFile.write("\t\t\tKmat__num_channels = " + str(res.Kmat__num_channels) + ",\n")

            # channel used to get the amplitude
            outFile.write("\t\t\tKmat__channel = " + str(res.Kmat__channel) + ",\n")

            # resonance masses
            outFile.write("\t\t\tKmat__resonance_masses = [\n")
            
            for mass in res.Kmat__resonance_masses :

                resonance_mass = 0

                # let's see if the current mass is a tensor or just a complex number
	        try :
                    resonance_mass = sess.run(mass)
                except :
                    resonance_mass = mass
                
                outFile.write("\t\t\t" + str(resonance_mass) + ",\n")

            outFile.write("\t\t\t],\n")

            # alpha couplings
            outFile.write("\t\t\tKmat__alpha_couplings = [\n")
            
            for i in res.Kmat__alpha_couplings :

                coupling = 0

                # let's see if the current coupling is a tensor or just a complex number
                try :
                    coupling = sess.run(i[0])
                except :
                    coupling = i[0]

                outFile.write("\t\t\t[" + str(coupling) + "],\n")

            outFile.write("\t\t\t],\n")

            # g couplings
            outFile.write("\t\t\tKmat__g_couplings = [\n")

            for i_coupling in res.Kmat__g_couplings :

                outFile.write("\t\t\t[\n")
                
                for j in i_coupling :
                    
                    coupling = 0
                    
                    # let's see if the current coupling is a tensor or just a complex number
                    try :
                        coupling = sess.run(j)
                    except :
                        coupling = j
                        
                    outFile.write("\t\t\t" + str(coupling) + ",\n")

                outFile.write("\t\t\t],\n")
                
            outFile.write("\t\t\t],\n")

            # widths factors of the sigma vector
            outFile.write("\t\t\tKmat__width_factors = [\n")
            
	    for i, width in enumerate(res.Kmat__width_factors) :

                width_value = 0

                # let's see if the current width is just a float or a tensor
                try :
                    width_value = sess.run(width)
                except :
                    width_value = width

                outFile.write("\t\t\t" + str(width_value) + ",\n")

            outFile.write("\t\t\t],\n")
                    
            # daughters masses
            outFile.write("\t\t\tKmat__daughters_masses = [\n")

            for i in res.Kmat__daughters_masses :
                
                # two daughters per channel
                outFile.write("\t\t\t[" + str(i[0]) + ", " + str(i[1]) + "],\n")

            outFile.write("\t\t\t],\n")

            # orbital momenta of the decays
            outFile.write("\t\t\tKmat__L_channels = [")

            for i in res.Kmat__L_channels :
                outFile.write(str(i) + ", ")

            outFile.write("],\n")

            # background terms
	    outFile.write("\t\t\tKmat__background_terms = [")

            for i in res.Kmat__background_terms :

                background_term = 0

                # let's see if the term is a tensor or just a complex number
                try :
                    background_term = sess.run(i)
                except :
                    background_term = i

                outFile.write(str(background_term) + ", ")

            outFile.write("],\n")
            
    else :

        # generic representation of the couplings

        outFile.write("\t\tgeneric_couplings_dict = {\n")

        # loop over the helicity amplitudes of the M decay
        for key_1 in res.genericHelAmp_M.keys() :
            
            try :
                if (type(res.genericHelAmp_M[key_1].comp_1) is Optimisation.FitParameter) and for_replacing :
                    outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1].comp_1_name
                                  + "' : Const(" + res.genericHelAmp_M[key_1].comp_1_name + "_VALUE),\n")
                else :
                    outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1].comp_1_name
                                  + "' : Const(" + str(sess.run(res.genericHelAmp_M[key_1].comp_1)) + "),\n")

                if (type(res.genericHelAmp_M[key_1].comp_2) is Optimisation.FitParameter) and for_replacing :
                    outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1].comp_2_name
                                  + "' : Const(" + res.genericHelAmp_M[key_1].comp_2_name + "_VALUE),\n")
		else :
                    outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1].comp_2_name
                                  + "' : Const(" + str(sess.run(res.genericHelAmp_M[key_1].comp_2)) + "),\n")
            except :
                for key_2 in res.genericHelAmp_M[key_1].keys() :

                    if (type(res.genericHelAmp_M[key_1][key_2].comp_1) is Optimisation.FitParameter) and for_replacing :
                        outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1][key_2].comp_1_name
                                      + "' : Const(" + res.genericHelAmp_M[key_1][key_2].comp_1_name + "_VALUE),\n")
                    else :
                        outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1][key_2].comp_1_name
                                      + "' : Const(" + str(sess.run(res.genericHelAmp_M[key_1][key_2].comp_1)) + "),\n")
                        
                    if (type(res.genericHelAmp_M[key_1][key_2].comp_2) is Optimisation.FitParameter) and for_replacing :
			outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1][key_2].comp_2_name
                                      + "' : Const(" + res.genericHelAmp_M[key_1][key_2].comp_2_name + "_VALUE),\n")
                    else :
                        outFile.write("\t\t\t'" + res.genericHelAmp_M[key_1][key_2].comp_2_name
                                      + "' : Const(" + str(sess.run(res.genericHelAmp_M[key_1][key_2].comp_2)) + "),\n")

        try :
            if (type(res.genericHelAmp_M[key_1][key_2].comp_1) is Optimisation.FitParameter) and for_replacing :
                outFile.write("\t\t\t'" + res.genericHelAmp_res.comp_1_name
                              + "' : Const(" + res.genericHelAmp_res.comp_1_name + "_VALUE),\n")
            else :
                outFile.write("\t\t\t'" + res.genericHelAmp_res.comp_1_name
                              + "' : Const(" + str(sess.run(res.genericHelAmp_res.comp_1)) + "),\n")

            if (type(res.genericHelAmp_M[key_1][key_2].comp_2) is Optimisation.FitParameter) and for_replacing :
              outFile.write("\t\t\t'" + res.genericHelAmp_res.comp_2_name
                              + "' : Const(" + res.genericHelAmp_res.comp_2_name + "_VALUE),\n")
            else :
                outFile.write("\t\t\t'" + res.genericHelAmp_res.comp_2_name
                              + "' : Const(" + str(sess.run(res.genericHelAmp_res.comp_2)) + "),\n")
        except :
            # loop over the helicity amplitudes of the resonance decay
            for key_1 in res.genericHelAmp_res.keys() :
                
                try :
                    if (type(res.genericHelAmp_res[key_1].comp_1) is Optimisation.FitParameter) and for_replacing :
                        outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1].comp_1_name
                                      + "' : Const(" + res.genericHelAmp_res[key_1].comp_1_name + "_VALUE),\n")
                    else :
                        outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1].comp_1_name
                                      + "' : Const(" + str(sess.run(res.genericHelAmp_res[key_1].comp_1)) + "),\n")

                    if (type(res.genericHelAmp_res[key_1].comp_2) is Optimisation.FitParameter) and for_replacing :
                        outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1].comp_2_name
                                      + "' : Const(" + res.genericHelAmp_res[key_1].comp_2_name + "_VALUE),\n")
                    else :
                        outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1].comp_2_name
                                  + "' : Const(" + str(sess.run(res.genericHelAmp_res[key_1].comp_2)) + "),\n")
                except:
                    for key_2 in res.genericHelAmp_res[key_1].keys() :

                        if (type(res.genericHelAmp_res[key_1][key_2].comp_1) is Optimisation.FitParameter) and for_replacing :
                            outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1][key_2].comp_1_name
                                          + "' : Const(" + res.genericHelAmp_res[key_1][key_2].comp_1_name + "_VALUE),\n")
                        else :
                            outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1][key_2].comp_1_name
                                          + "' : Const(" + str(sess.run(res.genericHelAmp_res[key_1][key_2].comp_1)) + "),\n")

                        if (type(res.genericHelAmp_res[key_1][key_2].comp_2) is Optimisation.FitParameter) and for_replacing :
                            outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1][key_2].comp_2_name
                                          + "' : Const(" + res.genericHelAmp_res[key_1][key_2].comp_2_name + "_VALUE),\n")
                        else :
                            outFile.write("\t\t\t'" + res.genericHelAmp_res[key_1][key_2].comp_2_name
                                          + "' : Const(" + str(sess.run(res.genericHelAmp_res[key_1][key_2].comp_2)) + "),\n")
            
        outFile.write("\t\t},\n")
        
    # parity conservation
    outFile.write("\t\tresdecay_parity_conservation = " + str(res.resdecay_parity_conservation) + ",\n")
    outFile.write("\t\tcolour = " + str(res.colour) + ", linestyle = " + str(res.linestyle) + ", fillstyle = " + str(res.fillstyle) + "),\n\n")

# dump the parsed parameters
def DumpParsedParameters(parsed_parameters, outFile_name):

    # open the output file
    outFile = open(outFile_name, "w")
    
    # loop over the parsed parameters
    for param_name in parsed_parameters.keys() :
        outFile.write(param_name + "\t" + parsed_parameters[param_name] + "\n")

# dump the amplitude model to a text file
def DumpModel(A_res, B_res, C_res, non_res, M_d1_hel,
              parsed_parameters, outFile_name, for_replacing = False) :
    
    # open the output file
    outFile = open(outFile_name, "w")

    # import some libraries which are needed
    outFile.write("import sys\n\n")
    outFile.write("from Constants import *\n")
    outFile.write("import MatrixElement, Optimisation\n")
    outFile.write("from Interface import Const, CastComplex\n")
    outFile.write("from Optimisation import FitParameter\n\n")
    
    # data legend
    outFile.write("data_legend = 'Run I + Run II data'\n\n")
    
    # dump the non-resonant contribution
    if (type(non_res.nonres_size) is Optimisation.FitParameter) and for_replacing :
        outFile.write("non_res = MatrixElement.NonResonant(nonres_size = Const(" + non_res.nonres_size.par_name + "_VALUE),\n")
    else :
        outFile.write("non_res = MatrixElement.NonResonant(nonres_size = Const(" + str(sess.run(non_res.nonres_size)) + "),\n")

    outFile.write("\tcolour = " + str(non_res.colour) + ", linestyle = " + str(non_res.linestyle) + ", fillstyle = " + str(non_res.fillstyle) + ")\n\n")

    # write the parsed paramaters, and their operations
    outFile.write("parsed_parameters = {\n")

    # loop over the parsed parameters
    for param_name in parsed_parameters.keys() :
        outFile.write('\t"' + param_name + '" : "' + parsed_parameters[param_name] + '",\n')
        
    outFile.write("}\n\n")
    
    # dump the M and d1 polarisation factors
    if M_d1_hel is None :
        outFile.write("M_d1_hel = None\n")
    else :
        outFile.write("M_d1_hel = {}\n")
        outFile.write("M_d1_hel[1] = {}\n")

        if (type(M_d1_hel[1][1]) is Optimisation.FitParameter) and for_replacing :
            outFile.write("M_d1_hel[1][1] = Const(" + M_d1_hel[1][1].par_name + "_VALUE)\n")
        else :
            outFile.write("M_d1_hel[1][1] = Const(" + str(sess.run(M_d1_hel[1][1])) + ")\n")

        if (type(M_d1_hel[1][-1]) is Optimisation.FitParameter) and for_replacing :
            outFile.write("M_d1_hel[1][-1] = Const(" + M_d1_hel[1][-1].par_name + "_VALUE)\n")
        else :
            outFile.write("M_d1_hel[1][-1] = Const(" + str(sess.run(M_d1_hel[1][-1])) + ")\n\n")

        outFile.write("M_d1_hel[-1] = {}\n")

        if (type(M_d1_hel[-1][1]) is Optimisation.FitParameter) and for_replacing :
            outFile.write("M_d1_hel[-1][1] = Const(" + M_d1_hel[-1][1].par_name + "_VALUE)\n")
        else :
            outFile.write("M_d1_hel[-1][1] = Const(" + str(sess.run(M_d1_hel[-1][1])) + ")\n")

        if (type(M_d1_hel[-1][-1]) is Optimisation.FitParameter) and for_replacing :
            outFile.write("M_d1_hel[-1][-1] = Const(" + M_d1_hel[-1][-1].par_name + "_VALUE)\n")
        else :
            outFile.write("M_d1_hel[-1][-1] = Const(" + str(sess.run(M_d1_hel[-1][-1])) + ")\n\n")
        
    outFile.write("A = [\n")
    
    # loop over the A resonances
    for res in A_res :
        DumpResonance(res, outFile, for_replacing = for_replacing)

    outFile.write("]\n\n")

    outFile.write("B = [\n")

    # loop over the B resonances
    for res in B_res :
        DumpResonance(res, outFile, for_replacing = for_replacing)

    outFile.write("]\n\n")
    
    outFile.write("C = [\n")
    
    # loop over the C resonances
    for res in C_res :
        DumpResonance(res, outFile, for_replacing = for_replacing)

    outFile.write("]\n\n")
    
#######################

# plotting stuff, mainly used by the PlotFit script

def GetData(branches, filename, treename):

    # open the input file
    data_file = TFile.Open(filename)

    return Optimisation.ReadNTuple(data_file.Get(treename), branches)

def GetEntries(filename, treename):

    # open the input file and tree
    data_file = TFile.Open(filename)
    tree = data_file.Get(treename)

    # get the number of entries in the tree
    n = tree.GetEntries()
    
    # close the input file
    data_file.Close()
    
    return n

def StyleAxis(axis):
    axis.SetTitleFont(132)
    axis.SetLabelFont(132)
    axis.SetNdivisions(505)
    axis.SetTitleSize(0.065)
    axis.SetTitleOffset(0.80)
    axis.SetLabelSize(0.055)
    axis.SetLabelOffset(0.015)

def ScaleAxisFonts(axis, scale):
    axis.SetTitleSize(axis.GetTitleSize()*scale)
    axis.SetTitleOffset(axis.GetTitleOffset()/scale)
    axis.SetLabelSize(axis.GetLabelSize()*scale)
    axis.SetLabelOffset(axis.GetLabelOffset()/scale)

def SetXTitle(hist, title, unit = ""):
    title = "#font[132]{}"+title
    
    if len(unit) > 0:
        title += " #font[132]{}[" + unit + "]"

    hist.GetXaxis().SetTitle(title)

def SetYTitle(hist, unit = ""):
    binw = hist.GetXaxis().GetBinWidth(1)

    if binw > 10:
        binw = int(round(binw))
    else:
        binw = round(binw, int(-math.floor(math.log10(binw))))

    title = "#font[132]{}Candidates / (" + str(binw)

    if len(unit) > 0:
        title += " " + unit

    title += ")"
    hist.GetYaxis().SetTitle(title)

def StyleHist(hist, title, unit = ""):
    StyleAxis(hist.GetXaxis())
    StyleAxis(hist.GetYaxis())
    SetXTitle(hist, title, unit)
    SetYTitle(hist, unit)
    
def StyleCanvas(canv):
    canv.SetMargin(0.11,0.05,0.15,0.05)
    canv.SetTicks(1,1)

def getname(index, resonances):

    if index < len(resonances):
        return resonances[index].name
    else:
        return "non-resonant"

def StyleComponentHist(hist, index, resonances):

    # default values for the non-resonant contribution                                                                                                                              
    name = "non-resonant"
    linewidth = 2
    colour = 30
    linestyle = 9
    fillstyle = 0

    if index < len(resonances) :

        # pick up the current resonance                                                                                                                                         
        resonance = resonances[index]
        
        # set the histo style                                                                                                                                                   
        name = resonance.label
        linewidth = resonance.linewidth
        linestyle = resonance.linestyle
        fillstyle = resonance.fillstyle
        colour = resonance.colour

    hist.SetTitle(name)
    hist.SetName(name)
    hist.SetLineWidth(linewidth)
    hist.SetLineColor(colour)
    hist.SetLineStyle(linestyle)
    hist.SetFillColor(colour)
    hist.SetFillStyle(fillstyle)

def AddVariables_ToSample(x, phsp, variables, weighted = False):

    """""                                                                                                                                                                           
      Add the required variables to the sample
        x         : datapoint                                                                                                                                      
        phsp      : phase space
        variables : variables to add
        weighted  : to add the weight to the sample, assuming is the last element of the datapoint
   """""
    
    data = []
    
    for var in variables:
        data += [eval("phsp." + var[0] + "(x)")]
        
    # is the weight included in the dataset already?                                                                                                                                
    # Otherwise, add 1. as weight                                                                                                                                                   
    if weighted:
        data += [tf.transpose(x)[-1]]
    else:
        data += [Interface.Ones(tf.transpose(x)[-1])]
        
    return tf.transpose(data)



